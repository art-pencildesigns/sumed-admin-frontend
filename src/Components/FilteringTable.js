import React from "react";

class FilteringTable extends Component {
  render() {
    let { selectFromArray, filteringText } = this.state;
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.name) || regexp.test(item.email)
    );
    return (
      <form onSubmit={this.submitSelectingModal()}>
        <HeaderWithSearch
          title="Assign to a committee"
          placeholder="Search Committees"
          value={filteringText}
          onChange={this.change("filteringText")}
        />
        <table className="table mt-2">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Meetings</th>
              <th>Creation date</th>
            </tr>
          </thead>
          <tbody>
            {renderedselectFromArray.map(selectItem => {
              return (
                <tr
                  key={selectItem.id}
                  onClick={this.updateSelectingModal({ selectItem })}
                  className={"hand " + (selectItem.checked ? "checkedTR" : "")}
                >
                  <td>
                    <input
                      type="checkbox"
                      checked={selectItem.checked}
                      onChange={_ => false}
                    />
                  </td>
                  <td>{selectItem.name}</td>
                  <td>{selectItem.meetingsCount}</td>
                  <td>{selectItem.creationDate}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Assign
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }
}

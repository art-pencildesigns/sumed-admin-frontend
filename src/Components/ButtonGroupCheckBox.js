import React from "react";
import { isArray } from "util";

export default class ButtonGroupCheckBox extends React.Component {
  render() {
    const { texts, values, value, onChange } = this.props;

    return (
      <div className="btn-group btn-block" role="group">
        {texts.map((item, index) => {
          let className;
          if (isArray(value))
            className = value.includes(values[index])
              ? "btn btn-primary"
              : "btn btn-outline-primary";
          else className = value == values[index] ? "btn btn-primary" : "btn btn-outline-primary";

          return (
            <button key={index} className={className} onClick={onChange(values[index])}>
              {item}
            </button>
          );
        })}
      </div>
    );
  }
}

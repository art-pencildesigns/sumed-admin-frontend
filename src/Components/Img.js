import React from "react";
import { Avatar } from "../Helpers/Constants";

export default function Img(props) {
  return (
    <img
      className="col-12"
      {...props}
      src={props.src || Avatar}
      alt="image not found"
    />
  );
}

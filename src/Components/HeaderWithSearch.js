import React from "react";
export default function HeaderWithSearch(props) {
  return (
    <div className="row">
      <h2 className=" col-6">
        <span className="float-left">{props.title}</span>
      </h2>
      <div className="input-group input-group-lg col-6">
        <input
          type="text"
          className="custom form-control customModel"
          placeholder={props.placeholder}
          value={props.value}
          onChange={props.onChange}
        />
        <div className="input-group-prepend">
          <span className="input-group-text customModel searchIcon">
            <i className="fa fa-search"></i>
          </span>
        </div>
      </div>
    </div>
  );
}

import React from "react";
export default function Msg(props) {
  return (
    <div className="modal-body p-5">
      <img
        src={"images/" + (props.img || "correct.svg")}
        width="180"
        className="mr-auto ml-auto d-block pt-3"
      />
      <h3 className="font-weight-bold text-center pt-5 pb-3">
        <b>{props.title}</b>
      </h3>
      <div className="text-center">
        <h6 className="font-weight-bold text-muted">{props.subTitle}</h6>
      </div>

      {props.showButtons && (
        <div className="float-right">
          <button
            onClick={props.onClick("cancel")}
            className="btn btn-default p-3 btn-round mr-5 text-muted"
          >
            Cancel
          </button>
          <button
            onClick={props.onClick("remove")}
            className="btn btn-danger p-3 btn-round bright"
          >
            Remove <span className="fa fa-arrow-circle-right ml-4"></span>
          </button>
        </div>
      )}
    </div>
  );
}

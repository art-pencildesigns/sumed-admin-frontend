import React from "react";

export default function ExBox(props) {
  return <div className="card p-3 ExBox">{props.children}</div>;
}

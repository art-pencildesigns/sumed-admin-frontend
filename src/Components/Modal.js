import React from "react";
import PropTypes from "prop-types";

class Modal extends React.Component {
  render() {
    let {
      id,
      title,
      children,
      buttonsBlock,
      buttonClose,
      sm,
      md,
      lg
    } = this.props;
    const dialogClassName =
      "modal-dialog modal-dialog-centered " +
      (sm ? "modal-sm" : "") +
      (lg ? "modal-lg" : "") +
      (md ? "modal-md" : "");
    return (
      <div
        className="modal fade"
        id={id}
        tabIndex={-1}
        role="dialog"
        aria-labelledby={id + "Title"}
        aria-hidden="true"
      >
        <div className={dialogClassName} role="document">
          <div className="modal-content px-3 py-2 radius">
            <div className="modal-body p-0 pt-2">
              {children}
              <br />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  id: PropTypes.string.isRequired
};

export default Modal;

import React from "react";
import Button from "./Button";

export default function Accordion(props) {
  let { id, className, children, icon, type, parent, show, onClick } = props;
  id = "id-" + id;

  if (type == "advancedButton")
    return (
      <div
        className={className ? className : "btn btn-outline-dark btn-sm col-12"}
        data-toggle="collapse"
        data-target={"#" + id}
        aria-expanded="false"
        aria-controls={id}
      >
        {children}
      </div>
    );
  else if (type == "button")
    return (
      <Button
        className={className ? className : "btn btn-outline-dark btn-sm col-12"}
        type="button"
        data-toggle="collapse"
        data-target={"#" + id}
        aria-expanded="false"
        aria-controls={id}
        icon={icon}
        onClick={onClick}
      >
        {children}
      </Button>
    );
  else if (type == "panel") {
    className = "collapse " + (show ? "show" : "") + " " + className;
    return (
      <div className={className} id={id} data-parent={parent ? "#" + parent : ""}>
        <div className="card card-body size1 p-0">{children}</div>
      </div>
    );
  }
}

import React  from "react";
export default function ReminderModel({onSubmitHandler,onChangeSubject,onChangeBody,docs}) {
    return (
        <div className="modal fade" id="ReminderModal" role="dialog" aria-labelledby="ReminderModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Email Body</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form  onSubmit={onSubmitHandler}>
                            <div className="form-group">
                                <label className="col-form-label">Subject</label>
                                <input type="text" className="form-control" value="Reminder Email" id="recipient-name" onChange={onChangeSubject} />
                            </div>
                            <div className="form-group">
                                <label className="col-form-label">Body</label>
                                <textarea className="form-control" rows={9} id="message-text" onChange={onChangeBody} value={docs}/>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal"  >Close</button>
                        <button type="button" className="btn btn-primary" onClick={onSubmitHandler}>Send message</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

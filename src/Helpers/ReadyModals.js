import React from "react";
import { Modal } from "../Components";


export function RenderDeleteModal() {
    return <Modal id="confirmDeleteModal" lg>
        < div className="modal-body p-5" >
            <img
                src="images/incorrect.svg"
                width="180"
                className="mr-auto ml-auto d-block pt-3"
            />
            <h3 className="font-weight-bold text-center pt-5 pb-3">
                <b>Confirm</b>
            </h3>
            <div className="text-center">
                <h6 className="font-weight-bold text-muted">Are you sure you want to delete this item ?</h6>
            </div>

            <div className="text-center mt-4">
                <button
                    onClick={evt => {
                        window.$("#confirmDeleteModal").modal("hide");
                    }}
                    className="btn btn-default p-3 btn-round mr-5 text-muted"
                >
                    Cancel
        </button>
                <button
                    onClick={evt => {
                        window.deleteParams.parent.deleteRow({ id: window.deleteParams.listItem.id })(window.deleteParams.evt);
                        window.$("#confirmDeleteModal").modal("hide");
                    }}
                    className="btn btn-danger p-3 btn-round bright"
                > Remove <span className="fa fa-thumbs-up ml-2"></span>
                </button>
                <br /><br />
            </div>

        </div >
    </Modal >
}
import React, { Component } from "react";
import {
  animateCSS,
  removeClass,
  addClass,
  cloneObj,
  generateRandomCode
} from "../../Helpers/General";
import { Modal, Input, Button, HeaderWithSearch } from "../../Components";
import { TableView, GenerateTable1, GenerateTable2 } from "../TableView";
import { Link } from "react-router-dom";
import Header from "../Header";
import { Query, FrontendRoute, BackendRoute } from "../../Helpers/Constants";
import { checkPermission, isSuperAdmin } from "../../Helpers/HelperComponenets";
import history from "../../history";


export default class Activities extends Component {
  state = {
    ActivityInfo: {},
    list: [],
    newObj: {
      id: false,
      name: "",
      arName: "",
      code: "",
      parent_id: 0,
      show: 1,
      product: 1
    },
    searchKey:''
  };

  emptyObj = {
    id: false,
    name: "",
    arName: "",
    code: "",
    parent_id: 0,
    show: 1,
    product: 1
  };

  ShowCreationModal = ({ id }) => evt => {
    evt.preventDefault();
    if (!checkPermission("activities")) return;

    let code = generateRandomCode(22);
    this.setState({
      newObj: {
        ...this.emptyObj,
        code,
        parent_id: id
      }
    });
    window.$("#createModal").modal("show");
  };

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    if (!checkPermission("activities")) return;

    let promise = Query(`activity/${id}`, "delete");
    promise
      .then(res => {
        let { list } = this.state;
        animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
          list = list.filter(item => item.id != id);
          this.setState({ list });
        });
      })
      .catch(this.handleErrors);
  };

  showEditRow = ({ listItem }) => evt => {
    evt.preventDefault();
    let { newObj } = this.state;
    newObj.id = listItem.id;
    newObj.name = listItem.name;
    newObj.arName = listItem.arName;
    newObj.code = listItem.code;
    newObj.product = listItem.product;
    newObj.show = listItem.show;
    this.setState({ newObj });
    window.$("#editModal").modal("show");
  };

  editRow = () => evt => {
    evt.preventDefault();

    let { name, arName, code, product, show, id } = this.state.newObj;
    let promise = Query(`activity/${id}`, "put", {
      name,
      arName,
      code,
      product,
      show: show == "true",
      parent_id: 0
    });
    promise
      .then(results => {
        this.refreshList();
        this.setState({ newObj: this.emptyObj });
        window.$("#editModal").modal("hide");
      })
      .catch(this.handleErrors);
  };
  searchRow (){
    let searchKey = this.state.searchKey;
    let promise = Query(`activities?searchKey=`+searchKey, "get");
    promise
      .then(results => {
        let list = results.data.data;
        list = GenerateTable1(list, this);
        this.setState({ list });
      })
      .catch(this.handleErrors);
  };

  refreshList() {
    Query(`activities`).then(res => {
      let list = res.data.data;
      list = GenerateTable1(list, this);
      this.setState({ list });
    });
  }

  refreshSubList() {
    let promise2 = Query("admin-members").then(res => {
      let selectFromArray = res.data.data;
      selectFromArray = GenerateTable2(selectFromArray);
      this.setState({ selectFromArray });
    });
  }

  handleErrors(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  addRow = _ => evt => {
    evt.preventDefault();
    let { name, arName, code, parent_id, product, show } = this.state.newObj;
    let { newObj } = this.state;
    let promise = Query("create-activity", "post", {
      name,
      arName,
      code,
      show,
      parent_id,
      product
    });
    promise
      .then(results => {
        this.refreshList();
        this.setState({ newObj: this.emptyObj });
        window.$("#createModal").modal("hide");
      })
      .catch(this.handleErrors);
  };

  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { newObj, selectFromArray } = this.state;
    let admin_ids = selectFromArray
      .filter(item => item.checked)
      .map(item => {
        return item.id;
      });
    let promise = Query("crud-committee-member", "post", {
      committee_id: newObj.id,
      admin_ids,
      delete: false
    }).then(res => {
      this.refreshList();
      this.setState({ newObj: { ...newObj, step: 3 } });
    });
    // window.$("#createModal").modal("hide");
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) item.checked = !item.checked;
      return item;
    });
    this.setState({ selectFromArray });
  };

  async componentDidMount() {
    this.refreshList();
    // animateCSS("#RightContent", "bounceOutRight", _ => {
    //   animateCSS("#RightContent", "bounceInRight");
    // });
  }

  render() {
    let { list } = this.state;
    let listLength = list.length || 0;

    return (
      <div className="p-0 m-3 mt-5">
        {this.renderCreationModal()}
        {this.renderEditModal()}
        <div className="row">
          <Header
            icon="tools"
            title="Activities"
            subTitle={listLength + " Activities"}
          />

          <div className="col-lg-8 col-11">
            <div className="float-right">
              <span className="ml-5">
                <input
                  type="file"
                  accept=".xls,.xlsx"
                  className="d-none"
                  id="hiddenInput"
                  onChange={this.uploadFile()}
                />
                {/* <input type="file" className="d-none" id="hiddenInput" onChange={this.uploadFile()} /> */}
                <div className="btn-group mr-1">
                  <button
                    className="btn btn-outline-suceess btnCreate"
                    onClick={this.openUploadPanel()}
                  >
                    {" "}
                    <i className="fa fa-cloud-upload-alt mr-1"></i> Add Bulk
                  </button>
                  <button
                    type="button"
                    className="btn btn-outline-sucees btnCreate p-1 dropdown-toggle dropdown-toggle-split"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="sr-only">Toggle Dropdown</span>
                  </button>
                  <div className="dropdown-menu">
                    <a
                      className="dropdown-item"
                      href={BackendRoute + "admin-api/export/activities"}
                      target="blank"
                      download
                    >
                      Download Activities as .XLSX
                    </a>
                    <a
                      className="dropdown-item"
                      href={FrontendRoute + "Activities-Template.xlsx"}
                      download
                    >
                      Download .XLSX Template
                    </a>
                  </div>
                </div>
                

                <button
                  className="btn btn-outline-suceess btnCreate mt-0"
                  onClick={this.ShowCreationModal({ id: 0 })}
                >
                  <i className="fa fa-plus-square mr-1"></i> New Activity
                </button>
              </span>
            </div>
          </div>
        </div>
        <div className="col-12">
          <div className="input-group input-group-lg mt-3">
            <div className="input-group-prepend">
              <span className="input-group-text bg-white border0"> <i className="fa fa-search"></i></span>
            </div>
            <input type="text" className="form-control border0" placeholder="Search For A Activity" aria-label="Search For A Activity" aria-describedby="button-addon2"
              onChange={evt => {
                this.change("searchKey", evt.target.value)(evt);
              }} value={this.state.searchKey}
            />
          </div>
        </div>

        {list.length <= 0 ? (
          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg">
            <h5 className="text-muted">There are no activities yet</h5>
          </div>
        ) : (
          <TableView
            renderTableList={this.renderTableList.bind(this)}
            renderTableHead={this.renderTableHead.bind(this)}
          />
        )}
      </div>
    );
  }

  exportActivities(){
    let promise = Query("export/activities");
    promise.then(results => {
      console.log('file : ',results)
      
        // download(results.data, "activities", "xlsx");
      })
  }

  openUploadPanel = params => evt => {
    evt.preventDefault();
    if (!isSuperAdmin()) return;
    window.$("#hiddenInput").click();
  };
  uploadFile = params => evt => {
    evt.preventDefault();
    let files = evt.target.files;
    // for (var i = 0, file; file = files[i]; i++) {
    let formData = new FormData();
    formData.append("file", files[0]);
    Query("upload-activities", "post", formData)
      .then(res => {
        window.alert("Success");
        this.refreshList();
      })
      .catch(this.errorHandling);
  };

  errorHandling(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText, searchKey } = this.state;
    let { name, arName, description, product, code, show } = this.state.newObj;
    switch (params) {
      case "name":
        name = value;
        break;
      case "arName":
        arName = value;
        break;
      case "description":
        description = value;
        break;
      case "filteringText":
        filteringText = value;
        break;
      case "product":
        product = value;
        break;
      case "code":
        code = value;
        break;
      case "searchKey":
        searchKey = value;
        this.searchRow();
        break;
      case "show":
        show = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState({
      newObj: { ...newObj, name, arName, description, product, code, show },
      filteringText,searchKey
    });
  };

  renderEditModal() {
    let { newObj } = this.state;
    return (
      <Modal id="editModal" onSubmit={this.editRow()}>
        <form onSubmit={this.editRow()}>
          <h2 className="pb-4">Edit Activity</h2>

          <Input
            label="Activity Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel mb-3"
            required
          />

          <Input
            label="Arabic Name"
            type="text"
            value={newObj.arName}
            onChange={this.change("arName")}
            className="form-control customModel mb-3"
            required
          />

          <Input
            label="Activity Code"
            type="text"
            value={newObj.code}
            onChange={this.change("code")}
            className="form-control customModel mb-3"
          />

          <Input
            label="Activity Type"
            type="text"
            className="form-control customModel mb-3"
            hideInput={true}
          >
            <select
              className="form-control customModel mb-3"
              value={newObj.product}
              onChange={this.change("product")}
            >
              <option value="1">Product (Vendor) / Offer (Bidder)</option>
              <option value="0">Service (Service Provider)</option>
              {/* <option value="2">Offer (Bidder)</option> */}
            </select>
          </Input>

          <Input
            label="Visibility"
            type="text"
            className="form-control customModel mb-3"
            hideInput={true}
          >
            <select
              className="form-control customModel mb-3"
              value={newObj.show}
              onChange={this.change("show")}
            >
              <option value={true}>Shown</option>
              <option value={false}>Hidden</option>
            </select>
          </Input>

          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Save
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  renderSelectingModal() {
    let { selectFromArray, filteringText } = this.state;
    console.log({ selectFromArray });
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.name) || regexp.test(item.email)
    );
    return (
      <form onSubmit={this.submitSelectingModal()}>
        <HeaderWithSearch
          title="Add Members"
          value={filteringText}
          onChange={this.change("filteringText")}
        />
        <table className="table mt-2">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Role</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {renderedselectFromArray.map(selectItem => {
              return (
                <tr
                  key={selectItem.id}
                  onClick={this.updateSelectingModal({ selectItem })}
                  className={"hand " + (selectItem.checked ? "checkedTR" : "")}
                >
                  <td>
                    <input
                      type="checkbox"
                      checked={selectItem.checked}
                      onChange={_ => false}
                    />
                  </td>
                  <td>{selectItem.name}</td>
                  <td>{selectItem.role_name}</td>
                  <td>{selectItem.email}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Add Later
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Add and Invite
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderCreateObj() {
    let { newObj } = this.state;
    return (
      <form onSubmit={this.addRow()} className="p-3">
        <h2 className="pb-4">Create Activity</h2>
        <Input
          label="Activity Name"
          type="text"
          value={newObj.name}
          onChange={this.change("name")}
          className="form-control customModel mb-3"
          required
        />

        <Input
          label="Arabic Name"
          type="text"
          value={newObj.arName}
          onChange={this.change("arName")}
          className="form-control customModel mb-3"
          required
        />

        <Input
          label="Activity Code"
          type="text"
          value={newObj.code}
          onChange={this.change("code")}
          className="form-control customModel mb-3"
        />

        <Input
          label="Activity Type"
          type="text"
          className="form-control customModel mb-3"
          hideInput={true}
        >
          <select
            className="form-control customModel mb-3"
            value={newObj.product}
            onChange={this.change("product")}
          >
            <option value="1">Product (Vendor) / Offer (Bidder)</option>
            <option value="0">Service (Service Provider)</option>
            {/* <option value="2">Offer (Bidder)</option> */}
          </select>
        </Input>

        <Input
          label="Visibility"
          type="text"
          className="form-control customModel mb-3"
          hideInput={true}
        >
          <select
            className="form-control customModel mb-3"
            value={newObj.show}
            onChange={this.change("show")}
          >
            <option value="1">Shown</option>
            <option value="0">Hidden</option>
          </select>
        </Input>

        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Create
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderCreationModal() {
    let { newObj } = this.state;
    return <Modal id="createModal">{this.renderCreateObj()}</Modal>;
  }

  gotoView(id) {
    history.push({
      pathname: "/activities/" + id,
      search: ""
    });
  }

  renderLink(id, txt) {
    return (
      <td onClick={evt => this.gotoView(id)} className="hand">
        {" "}
        {txt}{" "}
      </td>
    );
  }

  viewRow = ({ listItem }) => evt => {
    evt.preventDefault();
    this.gotoView(listItem.id);
  };

  renderTableList() {
    let { list } = this.state;
    return (
      <tbody>
        {list.map((item, index) => {
          return (
            <tr key={index} id={"id-" + item.id}>
              {this.renderLink(
                item.id,
                <span>
                   {item.name} 
                </span>
              )}
              {this.renderLink(item.id, item.code)}
              {this.renderLink(
                item.id,
                (item.suppliersCount || "No") + " Suppliers"
              )}
              {this.renderLink(
                item.id,
                (item.childrenCount || "No") + " Sub Activities"
              )}
              {this.renderLink(
                item.id,
                item.show ? (
                  <span className="text-success">Yes</span>
                ) : (
                  <span className="text-danger">No</span>
                )
              )}
              {this.renderLink(
                item.id,
                item.product == 1 ? (
                  <span className="text-success">Product</span>
                ) : item.product == 0 ? (
                  <span className="text-info">Service</span>
                ) : (
                  <span className="text-primary">Offer</span>
                )
              )}
              <td>{item.op}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }
  renderTableHead() {
    return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Code</th>
          <th>Number Of Vendors</th>
          <th>Number Of Sub Activities</th>
          <th>Shown</th>
          <th>Type</th>
          <th>Actions</th>
        </tr>
      </thead>
    );
  }
}

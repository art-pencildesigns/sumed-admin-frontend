import React, { Component } from "react";
import { Query } from "../../Helpers/Constants";
import Header from "../Header";
import { Modal, Input } from "../../Components";
import { generateRandomCode, animateCSS } from "../../Helpers/General";
import { checkPermission } from "../../Helpers/HelperComponenets";

export default class ActivitiesShow extends Component {
  state = {
    ActivityInfo: {},
    newObj: {
      id: false,
      name: "",
      code: "",
      parent_id: 0,
      show: 1,
      product: 1,
      have_ceiling_value: false,
      ceiling_value: ""
    }
  };

  emptyObj = {
    id: false,
    name: "",
    arName: "",
    code: "",
    parent_id: 0,
    show: 1,
    product: 1,
    have_ceiling_value: false,
    ceiling_value: ""
  };

  other = false;

  ShowCreationModal = ({ id }) => evt => {
    evt.preventDefault();
    if (!checkPermission("activities")) return;

    let code = generateRandomCode(22);
    this.setState({
      newObj: {
        ...this.emptyObj,
        code,
        parent_id: id
      }
    });
    window.$("#createModal").modal("show");
  };

  refreshList() {
    let id = this.props.match.params.id;
    Query(`activity/${id}`).then(res => {
      let ActivityInfo = res.data;
      if (ActivityInfo.code == "other") this.other = true;
      this.setState({ ActivityInfo });
    });
  }

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    if (!checkPermission("activities")) return;
    let promise = Query(`activity/${id}`, "delete");
    promise.then(res => {
      this.refreshList();
    });
  };

  componentDidMount() {
    let id = (this.id = this.props.match.params.id);
    this.refreshList();
    // animateCSS("#RightContent", "bounceOutRight", _ => {
    //   animateCSS("#RightContent", "bounceInRight");
    // });
  }

  handleErrors(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  addRow = _ => evt => {
    evt.preventDefault();
    let { name, arName, code, parent_id, ceiling_value } = this.state.newObj;
    let { newObj } = this.state;
    let promise = Query("create-activity", "post", {
      name,
      arName,
      code,
      show: 1,
      parent_id,
      value: ceiling_value
    });
    promise
      .then(results => {
        this.refreshList();
        this.setState({ newObj: this.emptyObj });
        window.$("#createModal").modal("hide");
      })
      .catch(this.handleErrors);
  };

  editActivitye = _ => evt => {
    evt.preventDefault();
    let { name, arName, code, parent_id, id, ceiling_value } = this.state.newObj;
    let { newObj } = this.state;
    let promise;
    if (this.other) {
      promise = Query(`edit-other-activity/${id}`, "put", { other: name });
    } else {
      promise = Query(`activity/${id}`, "put", { name, arName, code, show: 1, parent_id, value: ceiling_value });
    }
    promise
      .then(results => {
        this.refreshList();
        this.setState({ newObj: this.emptyObj });
        window.$("#editModal").modal("hide");
      })
      .catch(this.handleErrors);
  };

  switchShown = activity => evt => {
    evt.preventDefault();
    if (!checkPermission("activities")) return;

    let { name, arName, code, parent_id, id, show } = activity;
    let promise = Query(`activity/${id}`, "put", {
      name,
      arName,
      code,
      show: !show,
      parent_id
    })
      .then(res => {
        this.refreshList();
      })
      .catch(this.handleErrors);
  };

  renderButton() {
    return (
      <>
        <span className="ml-5">
          <button className="btn btn-outline-suceess btnCreate mt-0">
            <i className="fa fa-plus-square mr-1"></i> New Activity
          </button>
        </span>
      </>
    );
  }

  render() {
    let { ActivityInfo } = this.state;
    if (!ActivityInfo || !ActivityInfo.children) return <div></div>;
    return (
      <div id="activities">
        {this.renderCreationModal()}
        {this.renderEditModal()}
        <br />
        <div className="row mb-2">
          <Header
            icon="tools"
            title={ActivityInfo.id + "- " + ActivityInfo.name + " - " + ActivityInfo.arName}
            back="Back to Activities"
            to="/activities"
            className="col-lg-6"
          />

          <div className="col-lg-6 col-11">
            <div className="float-right">
              <span className="ml-5">
                {!this.other &&
                  <button
                    className="btn btn-outline-suceess btnCreate mt-0"
                    onClick={this.ShowCreationModal(ActivityInfo)}
                  >
                    <i className="fa fa-plus-square mr-1"></i> New Sub Activity
                </button>
                }
              </span>
            </div>
          </div>
        </div>

        <div
          className="accordion border-0 md-accordion mt-4 mb-3 mx-5"
          id="accordionAct"
          role="tablist"
          aria-multiselectable="true"
        >
          {/* Accordion card */}
          {!ActivityInfo.children.length && <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg">
            <h5 className="text-muted">There are no Sub Activities in here, {!this.other &&
              <button
                className="btn btn-outline-suceess btnCreate mt-0"
                onClick={this.ShowCreationModal(ActivityInfo)}
              >
                <i className="fa fa-plus-square mr-1"></i> Create Sub Activity
                </button>
            }</h5>
          </div>}
          {ActivityInfo.children.map(mainLevel => {
            return (
              <div className="m-1 mb-4" key={mainLevel.id}>
                <div className="card border-0 firstUploadCollapse shadowBox">
                  {/* Card header */}
                  <div
                    className="card-header border-bottom-1 bg-white p-3"
                    role="tab"
                  >
                    <a
                      data-toggle="collapse"
                      data-parent="#accordionAct"
                      href={`.Acc-${mainLevel.id}`}
                      aria-expanded="true"
                    >
                      <h4 className="mb-2 hedAccordian">
                        {mainLevel.id}- {mainLevel.name}- {mainLevel.arName} {mainLevel.value && <small className="text-warning">(<b>Value: {mainLevel.value}</b>)</small>}

                        <i className="float-right fas fa-angle-down rotate-icon"></i>

                        <small className="dropdown show dropleft float-right mx-4">
                          <a
                            className="fa fa-ellipsis-v hand actionButton"
                            role="button"
                            id="dropdownMenuLink"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          ></a>

                          {this.renderDropDownContent(mainLevel)}
                        </small>
                      </h4>
                    </a>
                  </div>
                  {/* Card body */}
                  {!this.other &&
                    <div
                      className={`"collapse collapse borderd border-top-0 Acc-${mainLevel.id}`}
                      role="tabpanel"
                      aria-labelledby="headingOne1"
                      data-parent="#accordionAct"
                    >
                      <div className="card-body m-0">
                        <div className="addDetails">
                          <div className="container-fluid">
                            <div className="row mb-0">
                              <div className="col-lg-8 col-md-6 col-3">
                                <p> Details </p>
                              </div>
                              <div className="col-lg-2 col-md-4 col-5 text-center">
                                <p>Number of Vendors</p>
                              </div>
                              <div className="col-lg-2 col-4">
                                <p className="text-center">Actions</p>
                              </div>
                            </div>
                            <hr className="mt-1 mb-1" />

                            {mainLevel.children.map((level2, index2) => {
                              return (
                                <div key={index2}>
                                  <div className="row border-bottom-1">
                                    <div className="col-lg-8">
                                      <p className="mb-1">
                                        <i className={"fa mr-1 fa-" + (level2.collapse ? "minus-square" : "plus-square")} onClick={evt => {
                                          level2.collapse = !!!level2.collapse;
                                          this.setState({ mainLevel })
                                        }}></i>
                                        {level2.id}- {level2.name}- {level2.arName} {level2.value && <small className="text-warning">(<b>Value: {level2.value}</b>)</small>}
                                      </p>
                                    </div>
                                    <div className="col-lg-2">
                                      <p className="mb-1 text-center">
                                        {level2.suppliersCount}
                                      </p>
                                    </div>
                                    <div className="col-lg-2 col-2 text-center">
                                      <div className="dropdown show">
                                        {/* <a
                                        className="fa fa-plus hand mr-2"
                                        onClick={this.ShowCreationModal(level2)}
                                      ></a> */}

                                        <a
                                          title="Hide Activity"
                                          className={
                                            "fa hand mr-2 " +
                                            (level2.show
                                              ? "fa-eye"
                                              : "fa-eye-slash")
                                          }
                                          onClick={this.switchShown(level2)}
                                        ></a>

                                        <a
                                          className="fa fa-ellipsis-v hand"
                                          role="button"
                                          id="dropdownMenuLink"
                                          data-toggle="dropdown"
                                          aria-haspopup="true"
                                          aria-expanded="false"
                                        ></a>
                                        {this.renderDropDownContent(level2)}
                                      </div>
                                    </div>
                                    <div className="col-12">
                                      <hr className="m-1" />
                                    </div>
                                  </div>
                                  {level2.collapse && level2.children.map((level3, index3) => {
                                    return (
                                      <div key={index3}>
                                        <div className="row">
                                          <div className="col-lg-1" />
                                          <div className="col-lg-7 col-md-6 col-4">
                                            <p className="mb-1">
                                              <i className={"fa mr-1 fa-" + (level3.collapse ? "minus-square" : "plus-square")} onClick={evt => {
                                                level3.collapse = !!!level3.collapse;
                                                this.setState({ mainLevel })
                                              }}></i>
                                              {level3.id}- {level3.name}- {level3.arName} {level3.value && <small className="text-warning">(<b>Value: {level3.value}</b>)</small>}
                                            </p>
                                          </div>
                                          <div className="col-lg-2 col-4">
                                            <p className="mb-1 text-center">
                                              {level3.suppliersCount}
                                            </p>
                                          </div>
                                          <div className="col-lg-2 col-2 text-center">
                                            <div className="dropdown show">
                                              {/* <a
                                              className="fa fa-plus hand mr-2"
                                              onClick={this.ShowCreationModal(
                                                level3
                                              )}
                                            ></a> */}

                                              <a
                                                title="Hide Activity"
                                                className={
                                                  "fa hand mr-2 " +
                                                  (level3.show
                                                    ? "fa-eye"
                                                    : "fa-eye-slash")
                                                }
                                                onClick={this.switchShown(level3)}
                                              ></a>

                                              <a
                                                className="fa fa-ellipsis-v hand"
                                                role="button"
                                                id="dropdownMenuLink"
                                                data-toggle="dropdown"
                                                aria-haspopup="true"
                                                aria-expanded="false"
                                              ></a>

                                              {this.renderDropDownContent(level3)}
                                            </div>
                                          </div>
                                          <div className="col-lg-1" />
                                          <div className="col-lg-11">
                                            <hr className="m-1" />
                                          </div>
                                        </div>
                                        {level3.collapse && level3.children.map((level4, index4) => {
                                          return (
                                            <div className="row" key={index4}>
                                              <div className="col-lg-2" />
                                              <div className="col-lg-6 col-md-6 col-4">
                                                <p className="mb-1">
                                                  <i className={"fa mr-1 fa-" + (level4.collapse ? "minus-square" : "plus-square")} onClick={evt => {
                                                    level4.collapse = !!!level4.collapse;
                                                    this.setState({ mainLevel })
                                                  }}></i>
                                                  {level4.id}- {level4.name}- {level4.arName} {level4.value && <small className="text-warning">(<b>Value: {level4.value}</b>)</small>}
                                                </p>
                                              </div>
                                              <div className="col-lg-2 col-4">
                                                <p className="mb-1 text-center text-muted">
                                                  {level4.suppliersCount}
                                                </p>
                                              </div>
                                              <div className="col-lg-2 col-2 text-center">
                                                <div className="dropdown show">
                                                  <a
                                                    className="fa fa-plus hand mr-2"
                                                    onClick={this.ShowCreationModal(
                                                      level4
                                                    )}
                                                  ></a>

                                                  <a
                                                    className={
                                                      "fa hand mr-2 " +
                                                      (level4.show
                                                        ? "fa-eye"
                                                        : "fa-eye-slash")
                                                    }
                                                    onClick={this.switchShown(
                                                      level4
                                                    )}
                                                  ></a>

                                                  <a
                                                    className="fa fa-ellipsis-v hand"
                                                    role="button"
                                                    id="dropdownMenuLink"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false"
                                                  ></a>
                                                  {this.renderDropDownContent(
                                                    level4
                                                  )}
                                                </div>
                                              </div>

                                              <div className="col-lg-2" />
                                              <div className="col-lg-10">
                                                <hr className="m-1" />
                                              </div>
                                              {level4.collapse && level4.children.map(
                                                (level5, index5) => {
                                                  return (
                                                    <div
                                                      className="row col-12 m-0 p-0"
                                                      key={index5}
                                                    >
                                                      <div className="col-3"></div>
                                                      <div className="col-5">
                                                        <p className="mb-1">
                                                          {level5.id}- {level5.name}- {level5.arName}
                                                        </p>
                                                      </div>
                                                      <div className="col-2">
                                                        <p className="mb-1 text-center text-muted">
                                                          {level5.suppliersCount}
                                                        </p>
                                                      </div>
                                                      <div className="col-lg-2 col-2 text-center">
                                                        <div className="dropdown show">
                                                          <a
                                                            className={
                                                              "fa hand mr-2 " +
                                                              (level5.show
                                                                ? "fa-eye"
                                                                : "fa-eye-slash")
                                                            }
                                                            onClick={this.switchShown(
                                                              level5
                                                            )}
                                                          ></a>

                                                          <a
                                                            className="fa fa-ellipsis-v hand"
                                                            role="button"
                                                            id="dropdownMenuLink"
                                                            data-toggle="dropdown"
                                                            aria-haspopup="true"
                                                            aria-expanded="false"
                                                          ></a>

                                                          {this.renderDropDownContent(
                                                            level5
                                                          )}
                                                        </div>
                                                      </div>

                                                      <div className="col-lg-3" />
                                                      <div className="col-lg-9">
                                                        <hr className="m-1" />
                                                      </div>
                                                    </div>
                                                  );
                                                }
                                              )}
                                              {level4.collapse && (<div className="row col-12">
                                                <div className="col-3"></div>
                                                <div className="col-9">
                                                  <a
                                                    className="fa fa-plus hand activityCreationBox"
                                                    onClick={this.ShowCreationModal(
                                                      level4
                                                    )}
                                                  >
                                                    Add New Sub-Activity
                                                </a>
                                                </div>
                                              </div>)}
                                            </div>
                                          );
                                        })}
                                        {level3.collapse && (<div className="row">
                                          <div className="col-2"></div>
                                          <div className="col-10">
                                            <a
                                              className="fa fa-plus hand activityCreationBox"
                                              onClick={this.ShowCreationModal(
                                                level3
                                              )}
                                            >
                                              Add New Sub-Activity
                                          </a>
                                          </div>
                                        </div>)}
                                      </div>
                                    );
                                  })}
                                  {level2.collapse && (<div className="row">
                                    <div className="col-1"></div>
                                    <div className="col-11">
                                      <a
                                        className="fa fa-plus hand activityCreationBox"
                                        onClick={this.ShowCreationModal(level2)}
                                      >
                                        Add New Sub-Activity
                                    </a>
                                    </div>
                                  </div>)}
                                </div>
                              );
                            })}
                            <a
                              className="fa fa-plus hand activityCreationBox"
                              onClick={this.ShowCreationModal(mainLevel)}
                            >
                              Add New Sub-Activity
                          </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  }
                </div>
              </div>
            );
          })}
        </div>
      </div >
    );
  }


  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText } = this.state;
    let { name, arName, description, product, code, have_ceiling_value, ceiling_value } = this.state.newObj;
    switch (params) {
      case "name": name = value; break;
      case "arName": arName = value; break;
      case "description": description = value; break;
      case "filteringText": filteringText = value; break;
      case "product": product = value; break;
      case "code": code = value; break;
      case "have_ceiling_value": have_ceiling_value = !!!have_ceiling_value; break;
      case "ceiling_value": ceiling_value = ceiling_value = value; break;
      default: console.error("something went wrong");
    }
    this.setState({ newObj: { ...newObj, name, arName, description, product, code, have_ceiling_value, ceiling_value }, filteringText });
  };


  renderCreationModal() {
    return (
      <Modal id="createModal">
        {this.renderCreateObj()}
        {/* {newObj.step == 1 && this.renderCreateObj()} */}
        {/* {newObj.step == 2 && this.renderSelectingModal()} */}
        {/* {newObj.step == 3 && this.renderSuccessMessage()} */}
      </Modal>
    );
  }

  renderDropDownContent(activity) {
    let { newObj } = this.state;
    return (
      <div className="dropdown-menu">
        <a
          className="dropdown-item"
          href="#"
          onClick={evt => {
            evt.preventDefault();
            if (!checkPermission("activities")) return;

            this.setState({
              newObj: {
                id: activity.id,
                name: activity.name,
                arName: activity.arName,
                code: activity.code,
                parent_id: activity.parent_id,
                show: 1,
                ceiling_value: activity.value
              }
            });
            window.$("#editModal").modal("show");
          }}
        >
          <span className="fa fa-edit"></span>
          &nbsp; Edit
        </a>

        {!this.other &&
          <a
            className="dropdown-item"
            href="#"
            onClick={this.deleteRow({
              id: activity.id
            })}
          >
            <span className="fa fa-trash-alt text-danger"></span>
            &nbsp; Delete
        </a>
        }
      </div>
    );
  }

  renderEditModal() {
    let { newObj } = this.state;
    return (
      <Modal id="editModal">
        <form onSubmit={this.editActivitye()} className="p-3">
          <h2 className="pb-4">Edit Activity</h2>
          <Input
            label="Activity Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel"
            required
          />
          <Input
            label="Arabic Name"
            type="text"
            value={newObj.arName}
            onChange={this.change("arName")}
            className="form-control customModel"
            required
          />
          <Input
            label="Activity Code"
            type="text"
            value={newObj.code}
            onChange={this.change("code")}
            className="form-control customModel"
          />
          { newObj.ceiling_value && 
              <Input
                  label="Category Value in Millions"
                  type="number"
                  value={newObj.ceiling_value}
                  onChange={this.change("ceiling_value")}
                  className="form-control customModel mb-3"
              />
          }
          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Save
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  addNewActivity = (index, item) => evt => {
    evt.preventDefault();
    Query();
    console.log({ index, item });
  };

  renderCreateObj() {
    let { newObj } = this.state;
    let { have_ceiling_value } = newObj;
    return (
      <form onSubmit={this.addRow()} className="px-3">
        <h2 className="pb-4">New Activity</h2>
        <Input
          label="Activity Name"
          type="text"
          value={newObj.name}
          onChange={this.change("name")}
          className="form-control customModel mb-3"
          required
        />
        <Input
          label="Arabic Name"
          type="text"
          value={newObj.arName}
          onChange={this.change("arName")}
          className="form-control customModel mb-3"
          required
        />
        <Input
          label="Activity Code"
          type="text"
          value={newObj.code}
          onChange={this.change("code")}
          className="form-control customModel mb-3"
        />


        <div className="form-group row col-12">
          <label onClick={this.change("have_ceiling_value")} className="hand">
            <input
              type="checkbox"
              checked={newObj.have_ceiling_value}
              onChange={this.change("have_ceiling_value")}
            />
            &nbsp; Is it a category ?&nbsp;
              {newObj.have_ceiling_value ? "Yes" : "No"}
          </label>
          <span>
          </span>
        </div>
        {have_ceiling_value &&
          <Input
            label="Category Value in Millions"
            type="number"
            value={newObj.ceiling_value}
            onChange={this.change("ceiling_value")}
            className="form-control customModel mb-3"
          />
        }



        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white mt-1 text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Create
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

}

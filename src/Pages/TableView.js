import React, { useState, useEffect } from "react";
import { animateCSS } from "../Helpers/General";
import { Modal } from "../Components";
import axios from "axios";
import { APIRoutes } from "../Helpers/Constants";
import ReminderModel from "../Components/ReminderModel"
import SuppliersList from "./Suppliers/SuppliersList"
import { Query } from '..//Helpers/Constants';

export const TableView = (props) => {
  const [subject, setSubject] = useState()
  const [body, setBody] = useState()
  const [id, setId] = useState(undefined)
  const [docs, setDocs] = useState()
  const [needUpdate , setNeedUpdate] = useState(false);
  const onChangeHandler = (e) => {
    const newSub = e.target.value
    setSubject(newSub)
  }
  const onChangeHanderBody = (e) => {
    const newBody = e.target.value
    setBody(newBody)
  }
  const sendreminderEmail = (id, docs) => {
    setId(id)
    setDocs(docs)
    setBody(docs)
  }
  useEffect(()=>{
    setId(id)
  },[id])
  const onSubmitHandler = () => {
      if (needUpdate)
      {
        Query(`send-need-update-reminder`, "post", {
          id:id,
          subject:subject,
          body:body,
        }).then(res => {
          console.log('res',res);
  
          if(res.status===200){
            window.$("#ReminderModal").modal("hide");
          }
          window.alert("Sent Successfully");
        }).catch(res => {
          // let errors = res.response.data.data.errors;
          // if (!errors.length) {
          //   this.errorHandling(res);
          //   return;
          // }
          window.alert("Something wrong happend !");
  
          // window.alert(errors[0]);
          // window.location.reload();
        });
      }
      else 
      {
        Query(`send-reminder`, "post", {
          id:id,
          subject:subject,
          body:body,
        }).then(res => {
          console.log('res',res);
  
          if(res.status===200){
            console.log('dsdasd');
            window.$("#ReminderModal").modal("hide");
          }
          window.alert("Sent Successfully");
        }).catch(res => {
          // let errors = res.response.data.data.errors;
          // if (!errors.length) {
          //   this.errorHandling(res);
          //   return;
          // }
          window.alert("Something wrong happend !");
  
          // window.alert(errors[0]);
          // window.location.reload();
        });
      }

   }

  const TableContnet = new SuppliersList()

  // render() {
  const { listHead, list } = props;
  // table-responsive
  return (
    <div className="table-responsive mt-3 shadowBox radius" id="Table">
      <table className="table bg-white m-0">
        {props.renderTableHead()}
        {props.renderTableList(sendreminderEmail,setNeedUpdate)}
      </table>
      { <ReminderModel needUpdate={needUpdate} onChangeSubject={onChangeHandler} onChangeBody={onChangeHanderBody} onSubmitHandler={onSubmitHandler} docs={docs}/>}
    </div>
  );
  // }
}

export function GenerateTable2(list) {
  list = list.map(listItem => {
    // if(contains)
    listItem.checked = false;
    return listItem;
  });
  return list;
}

export function GenerateTable3(list, containsArray) {
  list = list.map(listItem => {
    if (containsArray.includes(listItem.id)) listItem.checked = true;
    else listItem.checked = false;
    return listItem;
  });
  return list;
}

export function GenerateTable1(
  list,
  parent,
  options = { view: true, edit: true, delete: true }
) {
  list = list.map(listItem => {
    listItem.op = (
      <div>
        <div className="dropdown show">
          <a
            className="fa fa-ellipsis-v hand"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          ></a>

          <div
            className="dropdown-menu shadowBox"
            aria-labelledby="dropdownMenuLink"
          >
            {options.view && (
              <a
                className="dropdown-item"
                href="#"
                onClick={parent.viewRow({ listItem })}
              >
                <span className="fa fa-eye"></span> &nbsp; View
              </a>
            )}

            {options.edit && (
              <a
                className="dropdown-item"
                href="#"
                onClick={parent.showEditRow({ listItem })}
              >
                <span className="fa fa-edit"></span> &nbsp; Edit
              </a>
            )}

            {options.meetingResults && (
              <a
                className="dropdown-item"
                href="#"
                onClick={parent.meetingResults(listItem)}
              >
                <span className="fa fa-thumbs-up text-success"></span> &nbsp;
                Meeting Results
              </a>
            )}

            {options.completeMeeting && listItem.status != "completed" && (
              <a
                className="dropdown-item"
                href="#"
                onClick={parent.closeMeeting(listItem)}
              >
                <span className="fa fa-handshake text-danger"></span> &nbsp;
                Close Meeting
              </a>
            )}

            {options.delete && (
              <a
                className="dropdown-item"
                href="#"
                onClick={confirmDelete(listItem, parent)}
              >
                <span className="fa fa-trash-alt text-danger"></span> &nbsp;
                Delete
              </a>
            )}

          </div>
        </div>
      </div>
    );
    return listItem;
  });
  return list;
}

const confirmDelete = (listItem, parent) => evt => {
  evt.preventDefault();
  window.$("#confirmDeleteModal").modal("show");
  // console.log("damn", { listItem, parent });
  window.deleteParams = { parent, evt, listItem };
  // return;
}

export function GenerateTableRequests(
  list,
  parent,
  options = { edit: true, delete: true }
) {
  list = list.map(listItem => {
    listItem.op = (
      <div>
        <div className="dropdown show">
          <a
            className="fa fa-ellipsis-v hand"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          ></a>

          <div
            className="dropdown-menu shadowBox"
            aria-labelledby="dropdownMenuLink"
          >

            <a
              className="dropdown-item"
              href="#"
              onClick={parent.viewRow({ listItem })}
            >
              <span className="fa fa-eye"></span> &nbsp; View
            </a>

            {/* <a
              className="dropdown-item text-success"
              href="#"
              onClick={parent.requestAction("filtered", listItem)}
            >
              <span className="fa fa-check-circle"></span> &nbsp; Initial Approval
            </a>

            <a
              className="dropdown-item text-primary"
              href="#"
              onClick={parent.requestAction("assign", listItem)}
            >
              <span className="fa fa-check-double"></span> &nbsp; Add to Committee
            </a>

            <a
              className="dropdown-item text-danger"
              href="#"
              onClick={parent.requestAction("rejected", listItem)}
            >
              <span className="fa fa-times-circle"></span> &nbsp;
              Refuse Application
            </a> */}
          </div>
        </div>
      </div>
    );
    return listItem;
  });
  return list;
}

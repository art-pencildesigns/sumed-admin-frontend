import React, { Component } from "react";
import { Query, Avatar } from "../../Helpers/Constants";
import { updateUserData } from "../../Helpers/General";

export default class Login extends Component {
  state = {
    email: "",
    password: ""
  };

  errorHandling(error) {
    console.error({ error });
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }



  submitLogin = params => evt => {
    evt.preventDefault();
    let { email, password } = this.state;
    Query("login", "post", { email, password })
      .then(res => {
        res = res.data.data;
        console.log({ res })
        localStorage.setItem("access_token", res.access_token);
        localStorage.setItem("name", res.name);
        localStorage.setItem("phone", res.phone);
        localStorage.setItem("email", res.email);
        if (!res.image) res.image = Avatar;
        localStorage.setItem("image", res.image);
        localStorage.setItem("id", res.id);
        localStorage.setItem("role_id", res.role_id);
        updateUserData();
        setTimeout(() => {
          window.location.hash = "dashboard";
          window.location.reload();
        }, 1000);
      })
      .catch(this.errorHandling);
  };

  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { email, password } = this.state;
    switch (params) {
      case "email":
        email = value;
        break;
      case "password":
        password = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState({ email, password });
  };
  componentDidMount() {
    console.log('dddddddddddd');
  }
  render() {
    let { email, password } = this.state;
    
    return (
      <div className="login">
        <div className="container">
          <div className="row">
            <div className="col-lg-7">
              <div className="ml-3 txtLogin text-white py-4">
                <h1>WELCOME TO SUMED !</h1>
                <h4>Admin Portal</h4>
              </div>
            </div>
            <div className="col-lg-5">
              <div className="p-4">
                <form className="formLogin p-4" onSubmit={this.submitLogin()}>
                  <h3>Login</h3>
                  <div className="form-group mt-4">
                    <label className="font-weight-bold mb-1">
                      Email Address
                    </label>
                    <input
                      type="text"
                      className="form-control customModel"
                      placeholder="Email Address"
                      value={email}
                      onChange={this.change("email")}
                    />
                  </div>
                  <div className="form-group mb-2 mt-4">
                    <label className="font-weight-bold mb-1">Password</label>
                    <input
                      type="password"
                      className="form-control customModel"
                      placeholder="Password"
                      value={password}
                      onChange={this.change("password")}
                    />
                  </div>
                  {/* <a href="#">
                    <small>Reset Password</small>
                  </a> */}
                  <br />
                  <p className="text-right mt-4 mb-5">
                    <button className="btn btnLogin" type="submit">
                      Login <i className="far fa-arrow-alt-circle-right ml-5" />
                    </button>
                  </p>
                  <br />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

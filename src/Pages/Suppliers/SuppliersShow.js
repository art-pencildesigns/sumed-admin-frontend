import React, { Component } from "react";
import { Modal, Input, Button, HeaderWithSearch } from "../../Components";
import {
  TableView,
  GenerateTable1,
  GenerateTable2,
  GenerateTableRequests
} from "../TableView";
import Header from "../Header";
import { Query, BackendBase, BackendRoute } from "../../Helpers/Constants";
import { Not, checkPermission, getPermission } from "../../Helpers/HelperComponenets";
import ConfirmationMsg from "../../ConfirmationMsg";
import history from "../../history";
import { printFile, HandleErrors, updateUserData, supplierStatusClass, Naming } from "../../Helpers/General";
import Checkbox from "../../Components/Checkbox";

export default class SupplierShow extends Component {

  state = {
    supplierData: {},
    similarProjects: [],
    list: [],
    newObj: {
      id: false,
      name: "",
      description: "",
      step: 1,
      supplier: {}
    },
    selectFromArray: [],
    filteringText: "",
    emailTemplates: [],
    emailMsg: "",
  };

  id = null;
  confirmStatusFn = _ => { };
  actionActivity = false;

  async componentDidMount() {
    this.id = this.props.match.params.id;
    Query("email-templates", "get").then(res => {
      let emailTemplates = res.data.data;
      this.setState({ emailTemplates })
    }).catch(this.errorHandling)
    this.refreshList();
    // animateCSS("#RightContent", "bounceOutRight", _ => {
    //   animateCSS("#RightContent", "bounceInRight");
    // });
  }

  errorHandling(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  refreshSubList() {
    let promise2 = Query("committees").then(res => {
      let selectFromArray = res.data.data;
      selectFromArray = GenerateTable2(selectFromArray);
      this.setState({ selectFromArray });
    });
  }

  refreshList() {
    let promise = Query(`supplier/${this.id}`).then(res => {
      let supplierData = res.data.data;
      this.setState({ supplierData })
    })
  }

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    let promise;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) item.checked = true;
      else item.checked = false;
      return item;
    });
    this.setState({ selectFromArray });
  };

  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { newObj, selectFromArray, supplierData } = this.state;
    let committee = selectFromArray.filter(item => item.checked);
    if (!committee) {
      window.$("#createModal").modal("hide");
      return;
    }
    let committee_id = committee[0].id;
    let supplier_ids = [supplierData.id];

    Query(`supplier/${supplierData.id}`, "put", { status: "filtered" }).catch(
      this.errorHandling
    );

    // console.log({ committee_id, supplier: newObj.supplier.id });
    let promise = Query("crud-committee-suppliers", "post", {
      committee_id,
      supplier_ids,
      delete: false
    })
      .then(res => {
        updateUserData();
        setTimeout(() => {
          history.goBack();
        }, 500);
        window.$("#createModal").modal("hide");
        return;
      })
      .catch(this.errorHandling);
  };

  requestAction = (status) => evt => {
    evt.preventDefault();
    if (!checkPermission("requests")) return;

    let { newObj, supplierData, emailMsg } = this.state;
    let { id, supplier } = supplierData;
    if (status == "assign") {
      this.refreshSubList();
      this.setState({ newObj: { ...newObj, id, supplier } });
      window.$("#createModal").modal("show");
      return;
    }
    else if (status == "rejected") {
      // console.log("rejected")
      Query(`supplier/${id}`, "put", { status: "not submited", emailMsg }).then(_ => {
        window.$("#selectFromEmailTemplates").modal("hide");
        updateUserData();
        setTimeout(() => { history.goBack(); }, 500);
        return;
      }).catch(HandleErrors);
      return;
    } else if (status == "filtered") {
      Query(`supplier/${id}`, "put", { status: "filtered" }).then(_ => {
        updateUserData();
        setTimeout(() => { history.goBack(); }, 500);
        return;
      }).catch(HandleErrors);
    }
  };


  renderCreationModal() {
    let { selectFromArray, filteringText, newObj } = this.state;
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.name) || regexp.test(item.email)
    );
    return (
      <Modal id="createModal" lg>
        <form onSubmit={this.submitSelectingModal()}>
          <HeaderWithSearch
            title="Assign to a committee"
            placeholder="Search Committees"
            value={filteringText}
            onChange={this.change("filteringText")}
          />
          <table className="table mt-2">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Meetings</th>
                <th>Creation date</th>
              </tr>
            </thead>
            <tbody>
              {renderedselectFromArray.map(selectItem => {
                return (
                  <tr
                    key={selectItem.id}
                    onClick={this.updateSelectingModal({ selectItem })}
                    className={"hand " + (selectItem.checked ? "checkedTR" : "")}
                  >
                    <td>
                      <input
                        type="checkbox"
                        checked={selectItem.checked}
                        onChange={_ => false}
                      />
                    </td>
                    <td>{selectItem.name}</td>
                    <td>{selectItem.meetingsCount}</td>
                    <td>{selectItem.creationDate}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Assign
            <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>

      </Modal>
    );
  }


  render() {
    const pageType = this.props.pageType;
    const { supplierData, similarProjects } = this.state;
    if (!supplierData || !supplierData.id) return <div></div>;
    supplierData.documentsGrouped = Object.values(supplierData.documentsGrouped) || [];

    return (
      <div className="p-0 m-3 mt-5">
        {this.renderCreationModal()}
        {this.renderSelectFromEmailTemplates()}

        <ConfirmationMsg id="revisedConfirmationModal" msg="Are you sure you want to mark this request as Revised ?" confirmed={this.requestAction("filtered")} />
        <ConfirmationMsg id="requestRejectionModal" msg="Are you sure you want to reject this request ?" confirmed={this.requestAction("rejected")} />
        <ConfirmationMsg id="supplierBlacklistModal" msg="Are you sure you want to add this supplier to the Blacklist ?" confirmed={this.ControlBlacklist(supplierData.id)} />

        <ConfirmationMsg id="confirmChangeStatus" msg="Are you sure you want to change the activity status ?" confirmed={_ => this.confirmStatusFn(({ activity: this.actionActivity.id, action: this.actionActivity.action }))} />

        <div className="row">
          <div className="col-lg-12">
            <div className="row mt-2">
              <div className="col-4">
                {pageType == "supplier" ?
                  <Header
                    icon="hand-holding-box"
                    title={supplierData.username}
                    back="Back"
                    className=" "
                  /> :
                  <Header
                    icon="praying-hands"
                    title={supplierData.username}
                    back="Back to Requests"
                    to="/requests"
                    className=" "
                  />}
              </div>


              <div className="col-8 row">
                <div className="col-lg-12 customTab ">
                  <div className="btn-group float-right nav-pills nav-item" role="group" id="Tabs">
                    <button type="button" className="btn activeBtn nav-link px-4 mr-1 active" data-toggle="pill" href="#general" onClick={evt => {
                      window.$("#Tabs .nav-link.active").removeClass("active");
                      window.$(evt.currentTarget).addClass("active");
                    }}> <i className="fa fa-user-tie mr-1"></i> Details</button>
                    <button type="button" className="btn activeBtn nav-link px-4 mr-1" data-toggle="pill" href="#attachment" onClick={evt => {
                      window.$("#Tabs .nav-link.active").removeClass("active");
                      window.$(evt.currentTarget).addClass("active");
                    }}> <i className="fa fa-paperclip mr-1"></i> Attachment</button>
                    <button type="button" className="btn activeBtn nav-link px-4 mr-1" id="ActivitiesButton" data-toggle="pill" href="#Activities" onClick={evt => {
                      window.$("#Tabs .nav-link.active").removeClass("active");
                      window.$(evt.currentTarget).addClass("active");
                    }}> <i className="fa fa-list mr-1"></i> Activities</button>
                    <button type="button" className="btn activeBtn nav-link px-4 mr-1" data-toggle="pill" href="#logs" onClick={evt => {
                      evt.preventDefault();
                      window.$("#Tabs .nav-link.active").removeClass("active");
                      window.$(evt.currentTarget).addClass("active");
                    }}> <i className="fa fa-address-book mr-1"></i> Log</button>
                    { getPermission("suppliers") &&
                    <a href={`${BackendRoute}supplier/AdminLogin/${supplierData.username}`} target="_blank" className="btn activeBtn nav-link px-4 mr-1"> <i className="fa fa-edit mr-1"></i> Edit</a>
                  }
                    </div>
                </div>


                {pageType == "request" &&
                  <div className="col-lg-12 mt-4 mb-3">
                    <div className="float-right btn-group ">
                      <button className="btn btn-outline-primary nav-link px-4 mr-1" onClick={evt =>
                        window.$("#revisedConfirmationModal").modal("show")
                      }>
                        <i className="far fa-check-circle"> Revised</i>
                      </button>
                      <button className="btn btn-outline-primary nav-link px-4 mr-1" onClick={evt => {
                        window.$("#selectFromEmailTemplates").modal("show");
                      }}>
                        <i className="far fa-times-circle"> </i> Return to Registration <span className="badge badge-light">{supplierData.rejections}</span>
                      </button>
                      <button className="btn btn-outline-primary nav-link px-4 mr-1" onClick={this.requestAction("assign")}>
                        <i className="far fa-arrow-alt-circle-right"> Add to Committee</i>
                      </button>

                    </div>
                  </div>
                }
              </div>
            </div>








            <div className="tab-content mt-3">
              <div className="tab-pane show fade p-0 active" id="RequestShow">
                <div className="tab-content">
                  <div className="tab-pane show fade p-0 active" id="general">
                    <div className="row mt-4">
                      <div className="col-lg-8 general mb-3">
                        <h5 className="text-capitalize">General details:
                        (
                        <span className={supplierStatusClass(supplierData.status)}>{Naming(supplierData.status,
                          ["not submited", "filtered"],
                          ["Not Submitted", "Revised"]
                        )}</span >) 
                         { supplierData.status == 'frozen'  && <span >  Reason: there is some documents in upload document section has been expired{supplierData.reason} </span>}
                        
                          
                        </h5>
                        <div className="details bg-white p-3 mt-3">
                          <div className="row">
                            <div className="col-lg-6">
                              <p className="mb-1 pt-2 text-primary">Company Name</p>
                              <h6>{supplierData.company_name}</h6>
                              <p className="mb-1 pt-2 text-primary">Company Address</p>
                              <h6>{supplierData.address}</h6>

                              <p className="mb-1 pt-2 text-primary">Email</p>
                              <h6><a className="text-capitalize" href={"mailto:" + supplierData.email}>{supplierData.email}</a></h6>
                              <p className="mb-1 pt-2 text-primary">Fax</p>
                              <h6>{supplierData.fax_number}</h6>
                              <p className="mb-1 pt-2 text-primary">Type</p>
                              <h6 className="capitilize">{supplierData.type}</h6>
                              <p className="mb-1 pt-2 text-primary">Activities</p>

                              <a data-toggle="pill" href="#Activities" onClick={evt => {
                                evt.preventDefault();
                                window.$("#Tabs .nav-link.active").removeClass("active");
                                window.$("#ActivitiesButton").click();
                              }}>
                                <h6>{supplierData.ActivityNames.map((ac, i) =>
                                  <span key={i}>{ac.code == "other" ? ac[2] : ac[1]} <br /></span>
                                )}</h6>
                              </a>

                              {supplierData.lists.length > 0 && <div>
                                <p className="mb-1 pt-2 text-primary">Added To List: </p>
                                <h6>{supplierData.lists
                                  .map((list, ind) => <span className="hand text-warning" key={ind} onClick={evt => {
                                    evt.preventDefault();
                                    history.push({ pathname: `/list/${list.id}` });
                                  }}> {list.name}</span>)
                                  .reduce((prev, curr) => [prev, ' , ', curr])}</h6>
                              </div>}
                            </div>
                            <div className="col-lg-6">
                              <p className="mb-1 pt-2 text-primary">Commercial Name</p>
                              <h6>{supplierData.commercial_name}</h6>
                              <p className="mb-1 pt-2 text-primary">Mobile Number</p>
                              <h6>+{supplierData.phone_code} {supplierData.phone}</h6>
                              <p className="mb-1 pt-2 text-primary">City</p>
                              <h6>{supplierData.cityName} / {supplierData.countryName}</h6>
                              <p className="mb-1 pt-2 text-primary">Corporate Email</p>
                              <h6><a className="text-capitalize" href={"mailto:" + supplierData.corporate_email}>{supplierData.corporate_email}</a></h6>

                              <p className="mb-1 pt-2 text-primary">Scale</p>
                              <h6 className="capitilize">{supplierData.national}</h6>
                              <p className="mb-1 pt-2 text-primary">Website</p>
                              <h6>{supplierData.website}</h6>


                              {supplierData.reg_code && <div><p className="mb-1 pt-2 text-primary pt-2">Registration Code</p>
                                <h6>{supplierData.reg_code}</h6></div>
                              }
                              {supplierData.tax_number && <div><p className="mb-1 pt-2 text-primary pt-2">Tax Number</p>
                                <h6>{supplierData.tax_number}</h6></div>
                              }
                            </div>
                          </div>
                        </div>
                      </div>

                      {supplierData.contact_persons.map((contact, index) => {
                        return (<div className="col-lg-4 general" key={index}>
                          <h5>Contact Person</h5>
                          <div className="details bg-white p-3 mt-3">
                            <p className="mb-1">Name</p>
                            <h6 className="mt-1">{contact.name}</h6>
                            <p className="mb-1 pt-2">Mobile Number</p>
                            <h6 className="text-primary">
                              {contact.phones.map((phoneObj, index2) => {
                                return <div className="text-primary" key={index2}>+{phoneObj.code} {phoneObj.phone}</div>
                              })}
                            </h6>
                            <p className="mb-1 pt-2">Email</p>
                            <h6 className="mt-1">{contact.email}</h6>
                            <p className="mb-1 pt-2">Role</p>
                            <h6 className="mt-1">{contact.role}</h6>
                            <p className="mb-1 pt-2">Job Title</p>
                            <h6 className="mt-1">{contact.job}</h6>
                            <p className="mb-1 pt-2">Fax Number</p>
                            <h6 className="mt-1">{contact.fax}</h6>
                          </div>
                        </div>)
                      })}

                    </div>
                    <div className="mt-4">
                      <h5>Bank Details</h5>
                      <div className="accordion border-0 md-accordion mt-3 mb-3" id="BanksBox" role="tablist" aria-multiselectable="true">
                        {/* Accordion card */}
                        {(supplierData.banks <= 0) ?
                          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no Banks</h5></div>
                          : supplierData.banks.map((bank, index) => {
                            return (<div className="card border-0 firstUploadCollapse" key={index}>
                              {/* Card header */}
                              <div className="card-header border-bottom-0 bg-white p-3" role="tab">
                                <a data-toggle="collapse" data-parent="#BanksBox" href={`.bankID-${bank.id}`} aria-expanded="true">
                                  <h5 className="mb-2 hedAccordian border-0">
                                    {bank.abbreviation} - {bank.bank_name}
                                    <i className="float-right fas fa-angle-down rotate-icon" />
                                  </h5>
                                </a>
                              </div>
                              {/* Card body */}
                              <div className={"collapse borderd border-top-0 bankID-" + bank.id} role="tabpanel" aria-labelledby="headingOne1" data-parent="#BanksBox">
                                <div className="card-body">
                                  <div className="addDetails p-2">
                                    <div>
                                      <div className="row">
                                        <div className="col-lg-3">
                                          <h6> Account number </h6>
                                          <p>{bank.account_number} {bank.currency && <span><br />In: {bank.currency}</span>}</p>
                                        </div>
                                        <div className="col-lg-3">
                                          <h6>Country</h6>
                                          <p>{bank.countryName}</p>
                                        </div>
                                        <div className="col-lg-3">
                                          <h6>Swift Code</h6>
                                          <p>{bank.swift_code}</p>
                                        </div>
                                        <div className="col-lg-3">
                                          <h6>IBAN Code</h6>
                                          <p>{bank.iban_code}</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            )
                          })}

                      </div>
                    </div>



                    <div className="mt-4">
                      <h5>Branches</h5>
                      <div className="accordion border-0 md-accordion mt-3 mb-2" id="BranchsBox" role="tablist" aria-multiselectable="true">
                        {/* Accordion card */}

                        {(supplierData.branches <= 0) ?
                          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no Branches</h5></div>
                          : supplierData.branches.map((branch, index) => {
                            return <div className="card border-0 firstUploadCollapse" key={index}>
                              {/* Card header */}
                              <div className="card-header border-bottom-0 bg-white p-3" role="tab">
                                <a data-toggle="collapse" data-parent="#BranchsBox" href={`.branchID-${branch.id}`} aria-expanded="true" aria-controls="collapseDetails2">
                                  <h5 className="mb-2 hedAccordian border-0">
                                    {branch.name}
                                    <i className="float-right fas fa-angle-down rotate-icon" />
                                  </h5>
                                </a>
                              </div>
                              {/* Card body */}
                              <div className={"collapse borderd border-top-0 branchID-" + branch.id} role="tabpanel" aria-labelledby="headingOne1" data-parent="#BranchsBox">
                                <div className="card-body">
                                  <div className="addDetails p-2">
                                    <div>
                                      <div className="row">
                                        <div className="col-lg-3">
                                          <h6>Branch Address</h6>
                                          <p>{branch.address}</p>
                                        </div>
                                        <div className="col-lg-3">
                                          <h6>City / Country</h6>
                                          <p>{branch.cityName} / {branch.countryName}</p>
                                        </div>
                                        <div className="col-lg-3">
                                          <h6>Branch Email</h6>
                                          <p>{branch.email}</p>
                                        </div>
                                        <div className="col-lg-3">
                                          <h6> Branch Fax </h6>
                                          <p>{branch.fax}</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          })}
                      </div>
                    </div>




                    <div className="mt-4">
                      <h5>Agents</h5>
                      <div className="accordion border-0 md-accordion mt-3 mb-2" id="AgentBox" role="tablist" aria-multiselectable="true">
                        {/* Accordion card */}
                        {(supplierData.agents <= 0) ?
                          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no Agents</h5></div>
                          : supplierData.agents && supplierData.agents.map((agent, index) => {
                            return <div className="card border-0 firstUploadCollapse" key={index}>
                              {/* Card header */}
                              <div className="card-header border-bottom-0 bg-white p-3" role="tab">
                                <a data-toggle="collapse" data-parent="#AgentBox" href={`.agentID-${agent.id}`} aria-expanded="true" aria-controls="collapseDetails2">
                                  <h5 className="mb-2 hedAccordian border-0">
                                    {agent.name}
                                    <i className="float-right fas fa-angle-down rotate-icon" />
                                  </h5>
                                </a>
                              </div>
                              {/* Card body */}
                              <div className={"collapse borderd border-top-0 agentID-" + agent.id} role="tabpanel" aria-labelledby="headingOne1" data-parent="#AgentBox">
                                <div className="card-body">
                                  <div className="addDetails p-2">
                                    <div>
                                      <div className="row">
                                        <div className="col-lg-3">
                                          <h6>Agent Name</h6>
                                          <p>{agent.name}</p>
                                        </div>
                                        <div className="col-lg-3">
                                          <h6>Country</h6>
                                          <p>{agent.countryName}</p>
                                        </div>
                                        <div className="col-lg-3">
                                          <h6>Agent Email</h6>
                                          <p>{agent.email}</p>
                                        </div>
                                        <div className="col-lg-3">
                                          <h6>Agent Phone</h6>
                                          {agent.phones.map((phoneObj, index2) => {
                                            return <div className="text-primary" key={index2}>+{phoneObj.code} {phoneObj.phone}</div>
                                          })}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          })}
                      </div>
                    </div>


                    {!supplierData.blocked ?
                      <p className="mt-3"><a href="#" className="text-danger" onClick={evt => {
                        evt.preventDefault();
                        window.$("#supplierBlacklistModal").modal("show");
                      }}><i className="fa fa-times-circle" /> Add to Blacklist </a></p>
                      :
                      <p className="mt-3"><a href="#" className="text-danger" onClick={this.ControlBlacklist(supplierData.id, true)}><i className="fa fa-times-circle" /> Remove From Blacklist </a></p>
                    }




                  </div>
                  <div className="tab-pane p-0 fade" id="attachment">
                    <div className="accordion border-0 md-accordion mt-3" id="AttachmentsBox" role="tablist" aria-multiselectable="true">
                      {(supplierData.documentsGrouped <= 0) ?
                        <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no Attached Files</h5></div>
                        : supplierData.documentsGrouped.map((docBlock, index) => {
                          return (<div className="card border-0 firstUploadCollapse mb-2" key={index}>
                            <div className="card-header border-bottom-0 p-0 bg-white" role="tab">
                              <a data-toggle="collapse" data-parent="#AttachmentsBox" href={`.AttachmentBlock-${index}`} aria-expanded="true" aria-controls="collapseDetails2">
                                {(new Date(docBlock[0].expire_date) < new Date()) ? 
                                <h5 className="hedAccordian border-0 p-3 pb-0 m-0 text-danger">
                                  {docBlock[0].name}
                                  <i className="float-right fas fa-angle-down rotate-icon" />
                                  <p className="m-0"><small className="text-muted">(Start: {docBlock[0].start_time_processed} / Expiration: {docBlock[0].end_time_processed})</small></p>
                                </h5>
                                :
                                <h5 className="hedAccordian border-0 p-3 pb-0 m-0 ">
                                  {docBlock[0].name}
                                  <i className="float-right fas fa-angle-down rotate-icon" />
                                  <p className="m-0"><small className="text-muted">(Start: {docBlock[0].start_time_processed} / Expiration: {docBlock[0].end_time_processed})</small></p>
                                </h5>
                                }
                              </a>
                            </div>
                            <div className={"collapse borderd border-top-0 pt-0 AttachmentBlock-" + index} role="tabpanel" aria-labelledby="headingOne1" data-parent="#AttachmentsBox">
                              <div className="card-body pt-0">
                                <div className="addDetails px-1 py-0">
                                  {docBlock.map((file, index2) => {
                                    return (
                                      <div className="row mt-3" key={index2}>
                                        <a className="col-10 text-dark" href={file.path} download={file.name}>{file.name}</a>
                                        <div className="col-2">
                                          <a className="text-muted ml-2 hand" onClick={evt => printFile(file.path)}><i className="fa fa-print" /></a>
                                          <a className="text-dark ml-2" href={file.path} download={file.name} target="_blank"><i className="fa fa-download fDownload" /></a>
                                        </div>
                                      </div>)
                                  })}
                                </div>
                              </div>
                            </div>
                          </div>
                          )
                        })}
                    </div>
                  </div>



                  <div className="tab-pane fade p-0" id="logs">
                    {(supplierData.logs <= 0) ?
                      <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no Logs For this supplier</h5></div>
                      :
                      <div className="mt-4 p-4 logs shadowBox radius ">
                        {supplierData.logs.map((log, index) => {
                          return (<div className="row" key={index}>
                            {index >= 1 && <div className="col-12">
                              <hr className="mt-1" />
                            </div>}
                            <div className="col-lg-8">
                              <p>{log.content}</p>
                            </div>
                            <div className="col-lg-4">
                              <p className="float-right"><b>{log.creationDate}</b></p>
                            </div>
                          </div>
                          )
                        })}
                      </div>
                    }
                  </div>


                  <Modal id="showSimilarProjects" lg>
                    <h2 className="text-muted">Similar Projects</h2>
                    <div className="row p-2">

                      <div className="accordion border-0 md-accordion mt-3 col-12" id="SimilarProjectsBox" role="tablist" aria-multiselectable="true">
                        {similarProjects.map((project, index) => {
                          return (<div className="card border-0 firstUploadCollapse" key={index}>
                            <div className="card-header border-bottom-0 bg-white" role="tab">
                              <a data-toggle="collapse" data-parent="#SimilarProjectsBox" href={`.SimilarBlock-${index}`} aria-expanded="true" aria-controls="collapseDetails2">
                                <h5 className="mb-2 hedAccordian border-0">
                                  {project.name}
                                  <i className="float-right fas fa-angle-down rotate-icon" />
                                </h5>
                              </a>
                            </div>
                            <div className={"collapse borderd border-top-0 pt-0 SimilarBlock-" + index} role="tabpanel" aria-labelledby="headingOne1" data-parent="#SimilarProjectsBox">
                              <div className="card-body pt-0">
                                <div className="addDetails p-0 py-0">
                                  <div className="col col-sm-12 p-2" key={index}><div className="p-0 col-12">
                                    <h3 className="mb-3">{project.name}</h3>
                                    <h6 className="mb-1">Location</h6>
                                    <p className="mt-1">{project.location_name}</p>
                                    <h6 className="mb-1 pt-2">Mobile Number</h6>
                                    <p className="mt-1">{project.phone}</p>
                                    <h6 className="mb-1 pt-2">Title</h6>
                                    <p className="mt-1">{project.project_title}</p>
                                    <h6 className="mb-1 pt-2">Project Purpos</h6>
                                    <p className="mt-1">{project.project_purpos}</p>
                                    <h6 className="mb-1 pt-2">Project Scope</h6>
                                    <p className="mt-1">{project.scope}</p>

                                    {project.scope == "part" && <div className="">
                                      <h6 className="mb-1 pt-2">Part</h6>
                                      <p className="mt-1">{project.part}</p>
                                    </div>}
                                    {project.scope == "part" && < div >
                                      <h6 className="mb-1 pt-2">Awarded Value</h6>
                                      <p className="mt-1">{project.awarded_value}</p>
                                    </div>}
                                    {project.contract_value && <div>
                                      <h6 className="mb-1 pt-2">Contract Value</h6>
                                      <p className="mt-1">{project.contract_value}</p>
                                    </div>}
                                    
                                    
                                    {
                                      project.documents.length > 0 && <div>
                                        <h6 className="mb-3 pt-2">Uploaded Document</h6>
                                        {project.documents.map((file, index2) => {
                                            return(<p className="mt-1 line-height-50" key={index2}>
                                                  <a href={file.path} download={file.file} target="_blank">{file.file}</a>
                                                </p>);
                                          })
                                        }
                                      </div>
                                    }

                                  </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          )
                        })}
                      </div>
                    </div>
                  </Modal>
                  <div className="tab-pane fade p-0" id="Activities">
                    <div className="mt-4">
                      {(supplierData.ActivityNames <= 0) ?
                        <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no Activities For this supplier</h5></div>
                        :
                        <div className="bg-white details">
                          <table className="table bg-white table-hover border-top-0 mb-0">
                            <thead>
                              <tr className="border-0">
                                <th>Activity Name</th>
                                <th>Status</th>
                                <th>Ceiling Value</th>
                                <th>Similar Projects</th>
                              </tr>
                            </thead>
                            <tbody>
                              {supplierData.ActivityNames.map((activityArray, index) => {
                                return <tr key={index}>
                                  <td>{[activityArray[1], activityArray[2], activityArray[3], activityArray[4], activityArray[5], activityArray[6]].filter(item => item).join(" > ")}</td>
                                  <td className={"capitilize " + supplierStatusClass(activityArray[0])}>
                                    {activityArray[0]}
                                    {activityArray[0] == "rejected" && <span className="ml-1 btn btn-light p-1 hand" onClick={evt => {
                                      evt.preventDefault();
                                      debugger;
                                      this.confirmStatusFn = this.changeStatus;
                                      this.actionActivity = { id: activityArray.id, action: "approve" };
                                      window.$("#confirmChangeStatus").modal("show");
                                    }}>Change to Approved</span>}
                                    {activityArray[0] == "approved" && <span className="ml-1 btn btn-light p-1 hand" onClick={evt => {
                                      this.confirmStatusFn = this.changeStatus;
                                      this.actionActivity = { id: activityArray.id, action: "reject" };
                                      window.$("#confirmChangeStatus").modal("show");
                                    }}>Change to Rejected</span>}
                                  </td>
                                  <td>{activityArray.ceiling_value ? activityArray.ceiling_value + " Millions" : "No"}</td>
                                  <td>{activityArray.projects && activityArray.projects.length ?
                                    <button className="btn btn-primary btn-sm" onClick={_ => {
                                      window.$("#showSimilarProjects").modal("show");
                                      this.setState({ similarProjects: activityArray.projects });
                                    }}>Similar Projects <span className="badge badge-light">{activityArray.projects.length}</span> </button> :
                                    <span>No</span>
                                  }</td>
                                </tr>
                              })}
                            </tbody>
                          </table>
                        </div>
                      }
                    </div>
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div >
    );
  }

  changeStatus = ({ action, activity }) => {
    const { supplierData } = this.state;
    Query(`supplier-activity/${supplierData.id}`, "put", { activity, status: action }).then(res => {
      console.log(res);
      this.refreshList();
      // window.location.reload();
    })

  }

  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText, emailMsg } = this.state;
    let { name, description } = this.state.newObj;
    switch (params) {
      case "name":
        name = value;
        break;
      case "emailMsg":
        emailMsg = value;
        break;
      case "filteringText":
        filteringText = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState({ emailMsg, newObj: { ...newObj, name, description }, filteringText });
  };


  ControlBlacklist = (supplier_id, removeFromBlacklist = false) => evt => {
    Query("crud-list-supplier", "post", {
      "list_id": 1,
      "supplier_ids": [supplier_id],
      "delete": removeFromBlacklist
    }).then(res => {
      if (removeFromBlacklist) window.alert("Removed From Blacklist");
      else window.alert("Added to Blacklist");
      history.goBack();
      return;
    })
  }



  renderSelectFromEmailTemplates() {
    let { emailTemplates, actionType, emailMsg, supplierData } = this.state;
    emailTemplates = emailTemplates.filter(temp => {
      // if (actionType == "approved" && temp.category == "Supplier Approval") return true;
      // if (actionType == "rejected" && temp.category == "Supplier Rejection") return true;
      if (temp.category == "Supplier Request Refusal") return true;
      return false;
    });
    return <Modal id="selectFromEmailTemplates">
      <h4 className="mb-4">Write an email message or choose from a template: (In {supplierData.lang == "en" ? "English" : "Arabic"})</h4>
      {emailTemplates.length >= 1 && emailTemplates.map((temp, index) =>
        <button className="btn btn-outline-primary btn-block" key={index} onClick={evt => {
          this.setState({ emailMsg: (supplierData.lang == "en") ? temp.content : temp.content_ar })
        }}>{temp.name}</button>
      )}
      <h5 className="mt-4 mb-2">Why do you refuse to mark as revised ?</h5>
      <textarea rows="10" value={emailMsg} onChange={this.change("emailMsg")} className="form-control" placeholder="Enter Rejection Email Message"></textarea>

      <div className="modal-footer d-block px-5 border-top-0">
        <button
          type="button"
          className="bg-white text-muted border-0"
          data-dismiss="modal"
        >
          Cancel
            </button>
        <button
          className="btn btnContinue float-right"
          onClick={this.requestAction("rejected")}
        >
          Submit
              <i className="fa fa-arrow-circle-right ml-4"></i>
        </button>
      </div>
    </Modal>
  }

}


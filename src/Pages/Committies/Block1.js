import React from "react";
import { Link } from "react-router-dom";

export default props => {
  let committee = props.committee;
  return (
    <ul className="nav float-left customTab nav-pills float-right">
      <li className="nav-item mr-1">
        <Link
          className={
            "nav-link text-center px-3 " +
            (props.active == "summary" ? "active" : "")
          }
          to={"/committees/" + committee.id + "/summary"}
        >
          <i className="fa fa-th-list mr-2"></i>
          Summary
        </Link>
      </li>
    </ul >
  );
};

import React, { Component } from "react";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import { random_rgba, animateCSS, printFile, PrintContent } from "../../Helpers/General";
import { Link } from "react-router-dom";
import Block1 from "./Block1";
import Block2 from "./Block2";

export default class CommittySummary extends Component {
    state = {
        committee: {},
        summary: {},
        meeting: {}
    };

    async componentDidMount() {
        let committeeId = this.props.match.params.committeeId;
        let meetingId = this.props.match.params.meetingId;
        const promise1 = await Query(`committee/${committeeId}`);
        const promise2 = await Query(`meeting/${meetingId}`);
        const committee = promise1.data.data;
        const meeting = promise2.data.data;
        this.setState({ committee, meeting });

        window.$("#printingArea div").attr("contentEditable", true);
    };


    render() {
        let { committee, meeting } = this.state;
        if (!committee.id) return <div>No Data Yet !</div>
        return (
            <div className="p-0 m-3 mt-5">
                <div className="row">
                    <Header
                        icon="print"
                        className="col-6"
                        title="Print Invitation"
                        back="Meeting Information"
                        to={"/committees/" + committee.id + "/meetings/" + meeting.id}
                    />

                    <div className="col-6">
                        <div className="float-right">
                            <Link to={"/committees/" + committee.id + "/meetings/" + meeting.id} className="btn btn-outline-primary nav-link px-3 mr-1">
                                <i className="fa fa-arrow-circle-left mr-2"></i> Meeting
                            </Link>
                        </div>
                    </div>
                </div>



                <div className="mt-5 shadowBox bg-white radius" id="printingArea">
                    <div className="row p-5">
                        <div className="row col-12">
                            <div className="col-8">
                                <img src="images/sumed.png" />
                            </div>
                            <div className="col-4 float-right">
                                <p className="float-right text-center">Arab Petroleum Pipeline Company <br />Sumed</p>
                            </div>
                        </div>
                        <div className="row col-12">
                            <div className="mt-5 col-12 text-center line-height-50">
                                <h3 className="bold">Invitation</h3>
                                <p>To attend the meeting of the Standing Committee to update the records</p>
                                <p>(Supplier/Contractors/Bidders)</p>
                                <p>That invited by a decision of MR. Engineer / Chairman & managing Director</p>
                                <p className="text-danger">{committee.name}</p>
                            </div>
                        </div>
                        <div className="col-12"><hr /></div>
                        <div className="col-12 mt-5 mb-5">
                            <p>To the gentlemen</p>
                            <ol className="h5">
                                {committee.members.map((person, index) => {
                                    return <li key={index}>{person.name} - {person.roleName}</li>
                                })}
                            </ol>

                            <p className="h5 mt-5 line-height-150">Referring to resolution <span className="text-danger">{committee.name}</span> regarding the gathering of a committee to update the records
                        of (Suppliers/Contractors/Bidders).</p>

                            <p className="h5 mt-5 line-height-150">Please attend the meeting of the mentioned <span id="meetingInfo" className="text-danger">
                                at {meeting.start_time_processed} on {meeting.date_processed} at
                                the Meeting Room in the main building about the offer of (suppliers/contractors/bidders) with
                            the knowledge of the targeted company and for reviewing and getting the final notes.</span></p>
                        </div>

                        <div className="col-12"><hr /></div>
                        <div className="row col-12 mt-5">
                            <div className="col-6 text-center">
                                <small className="h6">Committee Chief</small>
                                <p className="bold h5">Ihab Oraby <br /> head of something</p>
                            </div>
                            <div className="col-6 text-center">
                                <small className="h6">Committee Organizer</small>
                                <p className="bold h5">Ihab Oraby <br /> head of somethingelse</p>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="col-12 mt-3">
                    <button className="btn btnSaveMeet float-right" onClick={evt => {
                        evt.preventDefault();
                        let content = window.document.getElementById("printingArea").innerHTML;
                        // console.log(content);
                        PrintContent(content);
                    }}>
                        Print <i className="fa fa-print ml-5" />
                    </button>
                    <button className="btn btnSaveMeet float-right mr-2" onClick={evt => {
                        evt.preventDefault();
                        window.location.reload();
                    }}>
                        Discard <i className="fa fa-times ml-5" />
                    </button>
                </div>


                <br />
                <br />
                <br />

            </div>

        );
    }
}

import React, { Component } from "react";
import { animateCSS, removeClass, addClass } from "../../Helpers/General";
import { Link } from "react-router-dom";

import { Modal, Input, Button, Msg } from "../../Components";
import { TableView, GenerateTable1, GenerateTable2 } from "../TableView";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import Block1 from "./Block1";
import history from "../../history";
import Block2 from "./Block2";

class Meetings2 extends Component {
  state = {
    list: [],
    newObj: {
      id: false,
      name: "",
      description: "",
      date: "",
      start_time: "",
      end_time: "",
      step: 1
    },
    committee: {
      id: this.props.match.params.id,
      name: ""
    },
    selectFromArray: [],
    filteringText: "",
    disabled:false
  };

  resetNewObj() {
    let newObj = {
      id: false,
      name: "",
      description: "",
      date: "",
      start_time: "",
      end_time: "",
      step: 1
    };
    this.setState({ newObj });
  }

  id = false;


  errorHandling(error) {
    
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    let promise = Query(`meeting/${id}`, "delete");
    promise.then(res => {
      let { list } = this.state;
      animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
        list = list.filter(item => item.id != id);
        this.setState({ list });
      });
    });
  };

  showEditRow = ({ listItem }) => evt => {
    evt.preventDefault();
    console.log({ listItem });
    let { newObj } = this.state;
    newObj.id = listItem.id;
    newObj.name = listItem.topic;
    newObj.date = listItem.date;
    newObj.start_time = listItem.start_time;
    newObj.end_time = listItem.end_time;
    this.setState({ newObj });
    window.$("#editModal").modal("show");
  };

  editRow = () => evt => {
    evt.preventDefault();
    //TODO: update the backend
    let { name, id, date, start_time, end_time } = this.state.newObj;
    let promise = Query(`meeting/${id}`, "put", { topic: name, date, start_time, end_time });
    promise
      .then(results => {
        this.refreshList();
        this.resetNewObj();
        window.$("#editModal").modal("hide");
      })
      .catch(this.errorHandling);
  };

  closeMeeting = meeting => evt => {
    evt.preventDefault();
    meeting.status = "completed";
    let uploadedFields = {
      // topic: meeting.topic,
      // start_time: meeting.start_time,
      // end_time: meeting.end_time,
      // date: meeting.date,
      // description: meeting.description,
      status: meeting.status
    }
    let promise = Query(`meeting/${meeting.id}`, "put", uploadedFields);
    promise.then(results => { this.refreshList(); }).catch(this.errorHandling);
  }

  meetingResults = meeting => evt => {
    evt.preventDefault();
    let uri = `committees/${this.state.committee.id}/meetings/${meeting.id}/results`
    window.location.hash = uri;
    return;
  }

  addRow = () => evt => {
    evt.preventDefault();
    this.setState({
      disabled : true
    });
    // let savebtn = $('#SaveMeeting');
    // savebtn.attr("disabled", true)
    let {
      name,
      description,
      date,
      start_time,
      end_time,
      topic
    } = this.state.newObj;
    // console.log({
    //   name,
    //   description,
    //   date,
    //   start_time,
    //   end_time
    // });
    // return;
    let { newObj, committee } = this.state;
    let promise = Query("add-meeting", "post", {
      topic: name,
      description,
      date,
      start_time,
      end_time,
      committee_id: committee.id
    });
    promise
      .then(results => {
        let promise = Query(`committee/${committee.id}/meetings`).then(res => {
          // savebtn.removeAttr("disabled")
          this.setState({
            disabled : false
          });
          this.resetNewObj();
          let list = res.data.data;
          this.refreshList();
          // let promise2 = Query("admin-members").then(res => {
          // let selectFromArray = res.data.data;
          // selectFromArray = GenerateTable2(selectFromArray);
          setTimeout(() => {
            this.setState({
              newObj: { ...newObj, step: 3 }
            });
          }, 300);
          // });
        });
      })
      .catch(this.errorHandling);
  };

  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { newObj } = this.state;
    this.setState({
      newObj: { ...newObj, step: 3 }
    });
    // window.$("#createModal").modal("hide");
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    let promise;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) {
        item.checked = !item.checked;
        let committee_id = 1;
        let admin_id = selectItem.id;

        if (item.checked) {
          promise = Query("add-committee-member", "post", {
            committee_id,
            admin_id
          });
        } else {
          promise = Query(
            `/committee/${committee_id}/member/${admin_id}`,
            "post"
          );
        }
        promise
          .then(res => {
            this.setState({ selectFromArray });
          })
          .catch(error => {
            console.log(error);
            window.alert("Error has happened");
          });
      }
      return item;
    });
  };

  refreshList() {
    let id = this.id = this.props.match.params.id;
    let promise = Query(`committee/${id}/meetings`).then(res => {
      let list = res.data.data;
      list = GenerateTable1(list, this, { view: true, edit: true, delete: true, meetingResults: false, completeMeeting: true });
      this.setState({ list });
    });
  }

  async componentDidMount() {
    let id = this.id = this.props.match.params.id;
    this.refreshList();
    const promise1 = await Query(`committee/${id}`);
    const committee = promise1.data.data;
    this.setState({ committee });
  }

  render() {
    let { list, committee } = this.state;
    let listLength = list.length || 0;
    return (
      <div className="p-0 m-3 mt-5">
        {this.renderCreationModal()}
        {this.renderEditModal()}
        <div className="row">
          <Header
            icon="handshake"
            className="col-4"
            title={committee.name}
            back="back to Committies"
            to="/committees"
          />

          <div className="col-8">
            <div className="float-right">
              {/* <img src="images/arr.png" />
              <img src="images/hed.png" /> */}

              <Block1 active="meetings" committee={committee} />

              <span className="ml-5">
                <button
                  className="btn btn-outline-suceess btnCreate mt-0 align-middle"
                  onClick={evt => {
                    evt.preventDefault();
                    this.resetNewObj();
                    window.$("#createModal").modal("show");
                  }}
                >
                  <i className="fa fa-plus-square mr-1"></i> Add Meeting
                </button>
              </span>
            </div>
          </div>

          <div className="col-12">
            <Block2 active="meetings" committee={committee} />
          </div>
        </div>

        {(list.length <= 0) ?
          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no meetings yet</h5></div>
          :
          <TableView
            renderTableList={this.renderTableList.bind(this)}
            renderTableHead={this.renderTableHead.bind(this)}
          />
        }
      </div>
    );
  }

  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText } = this.state;
    let { name, description, date, start_time, end_time } = this.state.newObj;
    switch (params) {
      case "name":
        name = value;
        break;
      case "description":
        description = value;
        break;
      case "date":
        date = value;
        break;
      case "start_time":
        start_time = value;
        break;
      case "end_time":
        end_time = value;
        break;
      case "filteringText":
        filteringText = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState({
      disabled : false
    });
    this.setState({
      newObj: { ...newObj, name, description, date, start_time, end_time },
      filteringText
    });
  };

  renderEditModal() {
    let { newObj } = this.state;
    return (
      <Modal id="editModal" onSubmit={this.editRow()}>
        <form onSubmit={this.editRow()}>
          <h2 className="pb-4">Edit Meeting</h2>
          <Input
            label="Meeting Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel mb-2"
            required
          />
          <Input
            label="Meeting Date"
            type="date"
            value={newObj.date}
            onChange={this.change("date")}
            className="form-control customModel mb-2"
            required
          />
          <Input
            label="Meeting Start Time"
            type="time"
            value={newObj.start_time}
            onChange={this.change("start_time")}
            className="form-control customModel mb-2"
            required
          />
          <Input
            label="Meeting End Time"
            type="time"
            value={newObj.end_time}
            onChange={this.change("end_time")}
            className="form-control customModel"
            required
          />
          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Save
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  renderSelectingModal() {
    let { selectFromArray, filteringText } = this.state;
    console.log({ selectFromArray });
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.name) || regexp.test(item.email)
    );
    return (
      <form onSubmit={this.submitSelectingModal()}>
        <h2 className="mb-4">
          <span className="float-left">Add Members</span>
          <input
            type="text"
            className="form-control float-right"
            placeholder="Search selectFromArray"
            value={filteringText}
            onChange={this.change("filteringText")}
          />
        </h2>
        <table className="table mt-2">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Role</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {renderedselectFromArray.map(selectItem => {
              return (
                <tr
                  key={selectItem.id}
                  onClick={this.updateSelectingModal({ selectItem })}
                  className="hand"
                >
                  <td>
                    <input
                      type="checkbox"
                      checked={selectItem.checked}
                      onChange={_ => false}
                    />
                  </td>
                  <td>{selectItem.name}</td>
                  <td>{selectItem.role_name}</td>
                  <td>{selectItem.email}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Add Later
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Add and Invite
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderCreateObj() {
    let { newObj } = this.state;
    return (
      <form onSubmit={this.addRow()} className="p-3">
        <h2 className="pb-4">
          New Meeting <br />
          {/* <small className="text-muted smallFont">office committtee</small> */}
          {/* TODO: attach committee name to this string & then show committee introduction box up on the listing page */}
        </h2>
        <Input
          label="Meeting Name"
          type="text"
          value={newObj.name}
          onChange={this.change("name")}
          className="form-control customModel"
          required
        />
        <Input
          label="Meeting Date"
          type="date"
          value={newObj.date}
          onChange={this.change("date")}
          className="form-control customModel"
          required
        />
        <Input
          label="Meeting Start Time"
          type="time"
          value={newObj.start_time_processed}
          onChange={this.change("start_time")}
          className="form-control customModel"
          required
        />
        <Input
          label="Meeting End Time"
          type="time"
          value={newObj.end_time_processed}
          onChange={this.change("end_time")}
          className="form-control customModel"
          required
        />
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            id="SaveMeeting"
            disabled={this.state.disabled}
            className="btn btnContinue float-right"
            type="submit"
          >
            Create Meeting
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderSuccessMessage() {
    let numberOfSelected = this.state.selectFromArray.filter(
      item => item.checked
    );
    return (
      <div className="modal-body p-5">
        <img
          src="images/correct.svg"
          className="imgSuccess mr-auto ml-auto d-block pt-3"
        />
        <h4 className="font-weight-bold text-center pt-5 pb-3">
          Meeting has been added successfully
        </h4>
        <p className="text-center">
          <small className="font-weight-bold text-muted">
            {numberOfSelected.length} Members in the Committee
          </small>
        </p>
      </div>
    );
  }

  renderCreationModal() {
    let { newObj } = this.state;
    return (
      <Modal id="createModal">
        {newObj.step == 1 && this.renderCreateObj()}
        {/* {newObj.step == 2 && this.renderSelectingModal()} */}
        {newObj.step == 3 && (
          <Msg title="Meeting has been added successfully" />
        )}
      </Modal>
    );
  }



  gotoView(id) {
    let committee_id = this.state.committee.id;
    history.push({
      pathname: `/committees/${committee_id}/meetings/${id}`,
      search: ''
    })
  }
  renderLink(id, txt) {
    return (
      <td onClick={evt => this.gotoView(id)} className="hand"> {txt} </td>
    );
  }
  viewRow = ({ listItem }) => evt => {
    evt.preventDefault();
    this.gotoView(listItem.id);
  }

  renderTableList() {
    let { list, committee } = this.state;
    return (
      <tbody>
        {list.map((item, index) => {
          let statusClass = "text-capitalize ";
          if (item.status == "pending") statusClass += "text-muted";
          else if (item.status == "completed") statusClass += "text-success";
          else if (item.status == "inProgress") statusClass += "text-primary";
          return (
            <tr key={index} id={"id-" + item.id}>
              {this.renderLink(item.id, item.topic)}
              {this.renderLink(item.id, item.date_processed)}
              {this.renderLink(item.id, <span className="text-uppercase">{item.start_time_processed} - {item.end_time_processed}</span>)}
              {this.renderLink(item.id, item.suppliersCount)}
              {this.renderLink(item.id, <span className={statusClass}>{item.status}</span>)}
              <td>{item.op}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }
  renderTableHead() {
    return (
      <thead>
        <tr>
          <th>Topic</th>
          <th>Date</th>
          <th>Time</th>
          <th>Suppliers</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
    );
  }
}

export default Meetings2;

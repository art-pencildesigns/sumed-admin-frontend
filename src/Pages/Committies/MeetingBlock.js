import React, { Component } from "react";
import { Link } from "react-router-dom";

export function MeetingBlock(props) {
    return <div className="col-lg-8">
        <ul className="nav customTab btn-group float-right nav-pills nav-item">
            <li className="nav-item">
                <a data-toggle="pill" href="#details" className="btn activeBtn nav-link px-3 mr-1 active">
                    <i className="fa fa-handshake mr-2"></i> Details
        </a>
            </li>
            <li className="nav-item">
                <a data-toggle="pill" href="#attachments" className="btn activeBtn nav-link px-3 mr-1">
                    <i className="fa fa-download mr-2"></i> Attachments
          </a>
            </li>
            <li className="nav-item">
                <a data-toggle="pill" href="#meetingResults" className="btn activeBtn nav-link px-3 mr-1">
                    <i className="fa fa-thumbs-up mr-2"></i> Meeting Results
                </a>
            </li>
            <li className="nav-item">
                <Link to={"/committees/" + props.committeeId + "/print-members/" + props.meetingId} className="btn btn-outline-primary nav-link px-3 mr-1">
                    <i className="fa fa-print mr-2"></i> Print Invitation
                </Link>
            </li>
        </ul>
    </div>
}
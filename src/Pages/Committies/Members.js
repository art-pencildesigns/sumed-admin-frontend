import React, { Component } from "react";
import {
  animateCSS,
  removeClass,
  addClass,
  cloneObj
} from "../../Helpers/General";
import { Modal, Input, Button, Msg, HeaderWithSearch } from "../../Components";
import {
  TableView,
  GenerateTable1,
  GenerateTable2,
  GenerateTable3
} from "../TableView";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import Block1 from "./Block1";
import Img from "../../Components/Img";
import Block2 from "./Block2";

export default class Suppliers extends Component {
  state = {
    list: [],
    newObj: {
      id: false,
      name: "",
      description: "",
      date: "",
      start_time: "",
      end_time: "",
      step: 1
    },
    committee: {
      id: this.props.match.params.id,
      name: ""
    },
    selectFromArray: [],
    filteringText: ""
  };

  resetNewObj() {
    let newObj = {
      id: false,
      name: "",
      description: "",
      date: "",
      start_time: "",
      end_time: "",
      step: 1
    };
    this.setState({ newObj });
  }

  errorHandling(error) {
    console.log({ error });
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    let promise = Query(`crud-committee-member`, "post", {
      committee_id: this.state.committee.id,
      admin_ids: [id],
      delete: true
    });
    promise
      .then(res => {
        let { list } = this.state;
        animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
          list = list.filter(item => item.id != id);
          this.setState({ list });
        });
      })
      .catch(this.errorHandling);
  };

  showEditRow = ({ listItem }) => evt => {
    evt.preventDefault();
    this.resetNewObj();
    this.refreshSubList();
    window.$("#createModal").modal("show");
  };

  editRow = () => evt => {
    evt.preventDefault();
    //TODO: update the backend
    let { name, description } = this.state.newObj;
    let promise = Query("create-committee", "post", { name, description });
    promise
      .then(results => {
        let promise = Query("committees").then(res => {
          let list = res.data.data;
          list = GenerateTable1(list, this, { delete: true });
          this.setState({ list });
          this.resetNewObj();
          window.$("#editModal").modal("hide");
        });
      })
      .catch(this.errorHandling);
  };

  addRow = () => evt => {
    evt.preventDefault();
    let {
      name,
      description,
      date,
      start_time,
      end_time,
      topic
    } = this.state.newObj;
    // console.log({
    //   name,
    //   description,
    //   date,
    //   start_time,
    //   end_time
    // });
    // return;
    let { newObj, committee } = this.state;
    let promise = Query("add-meeting", "post", {
      topic: name,
      description,
      date,
      start_time,
      end_time,
      committee_id: committee.id
    });
    promise
      .then(results => {
        let promise = Query(`/committee/${committee.id}/members`).then(res => {
          this.resetNewObj();
          let list = res.data.data;
          list = GenerateTable1(list, this);
          // let promise2 = Query("admin-members").then(res => {
          // let selectFromArray = res.data.data;
          // selectFromArray = GenerateTable2(selectFromArray);
          setTimeout(() => {
            this.setState({
              list,
              newObj: { ...newObj, step: 3 }
            });
          }, 300);
          // });
        });
      })
      .catch(error => {
        console.error("request failed: ", error);
      });
  };

  refreshList() {
    let id = this.state.committee.id;
    let promise = Query(`committee/${id}/members`).then(res => {
      let list = res.data.data;
      list = GenerateTable1(list, this, { view: true, delete: true });
      this.setState({ list });
    });
  }
  refreshSubList() {
    let { list } = this.state;
    let promise2 = Query("admin-members").then(res => {
      let selectFromArray = res.data.data;
      let inThereMap = list.map(item => item.id);
      selectFromArray = GenerateTable3(selectFromArray, inThereMap);
      this.setState({ selectFromArray });
    });
  }

  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { newObj, selectFromArray, committee } = this.state;
    let admin_ids = selectFromArray
      .filter(item => item.checked)
      .map(item => {
        return item.id;
      });
    let promise = Query("crud-committee-member", "post", {
      committee_id: committee.id,
      admin_ids,
      delete: false
    })
      .then(res => {
        this.refreshList();
        this.setState({ newObj: { ...newObj, step: 2 } });
      })
      .catch(this.errorHandling);
    // window.$("#createModal").modal("hide");
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) item.checked = !item.checked;
      return item;
    });
    this.setState({ selectFromArray });
  };

  async componentDidMount() {
    let id = (this.id = this.props.match.params.id);
    const promise1 = await Query(`committee/${id}`);
    const committee = promise1.data.data;
    this.setState({ committee });
    this.refreshList();
  }


  render() {
    let { list, committee } = this.state;
    console.log({ committee });
    let listLength = list.length || 0;
    return (
      <div className="p-0 m-3 mt-5">
        {this.renderCreationModal()}
        {this.renderEditModal()}
        <div className="row">
          <Header
            icon="users-cog"
            title={committee.name}
            subTitle={listLength + " Members"}
            back="back to Committies"
            to="/committees"
          />

          <div className="col-lg-8">
            <div className="float-right">
              {/* <img src="images/arr.png" />
              <img src="images/hed.png" /> */}

              <Block1 active="members" committee={committee} />

              <span className="ml-5">
                <button
                  className="btn btn-outline-suceess btnCreate mt-0 align-middle"
                  onClick={evt => {
                    evt.preventDefault();
                    this.resetNewObj();
                    this.refreshSubList();
                    window.$("#createModal").modal("show");
                  }}
                >
                  <i className="fa fa-plus-square mr-1"></i> Add Members
                </button>
              </span>
            </div>
          </div>
          <div className="col-12">
            <Block2 active="members" committee={committee} />
          </div>
        </div>

        {(list.length <= 0) ?
          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no members yet</h5></div>
          :
          <TableView
            renderTableList={this.renderTableList.bind(this)}
            renderTableHead={this.renderTableHead.bind(this)}
          />
        }
      </div>
    );
  }

  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText } = this.state;
    let { name, description, date, start_time, end_time } = this.state.newObj;
    switch (params) {
      case "name":
        name = value;
        break;
      case "description":
        description = value;
        break;
      case "date":
        date = value;
        break;
      case "start_time":
        start_time = value;
        break;
      case "end_time":
        end_time = value;
        break;
      case "filteringText":
        filteringText = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState({
      newObj: { ...newObj, name, description, date, start_time, end_time },
      filteringText
    });
  };

  renderEditModal() {
    let { newObj } = this.state;
    return (
      <Modal id="editModal" onSubmit={this.editRow()} lg>
        <form onSubmit={this.editRow()}>
          <h2 className="pb-4">Edit Committee</h2>
          <Input
            label="Committee Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel"
          />
          <Input
            label="Committee Description"
            type="text"
            value={newObj.description}
            onChange={this.change("description")}
            className="form-control customModel"
          />
          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Save
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  renderSelectingModal() {
    let { selectFromArray, filteringText } = this.state;
    console.log({ selectFromArray });
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.name) || regexp.test(item.email)
    );
    return (
      <form onSubmit={this.submitSelectingModal()}>
        <HeaderWithSearch
          title="Add Members"
          value={filteringText}
          onChange={this.change("filteringText")}
        />
        <table className="table mt-2">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {renderedselectFromArray.map(selectItem => {
              return (
                <tr
                  key={selectItem.id}
                  onClick={this.updateSelectingModal({ selectItem })}
                  className={"hand " + (selectItem.checked ? "checkedTR" : "")}
                >
                  <td>
                    <input
                      type="checkbox"
                      checked={selectItem.checked}
                      onChange={_ => false}
                    />
                  </td>
                  <td>{selectItem.name}</td>
                  <td>{selectItem.phone}</td>
                  <td>{selectItem.email}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Save Committee Members
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderCreateObj() {
    let { newObj } = this.state;
    return (
      <form onSubmit={this.addRow()} className="p-3">
        <h2 className="pb-4">
          New Meeting <br />
          {/* <small className="text-muted smallFont">office committtee</small> */}
          {/* TODO: attach committee name to this string & then show committee introduction box up on the listing page */}
        </h2>
        <Input
          label="Meeting Name"
          type="text"
          value={newObj.name}
          onChange={this.change("name")}
          className="form-control customModel"
          required
        />
        <Input
          label="Meeting Date"
          type="date"
          value={newObj.date}
          onChange={this.change("date")}
          className="form-control customModel"
          required
        />
        <Input
          label="Meeting Start Time"
          type="time"
          value={newObj.start_time}
          onChange={this.change("start_time")}
          className="form-control customModel"
          required
        />
        <Input
          label="Meeting End Time"
          type="time"
          value={newObj.end_time}
          onChange={this.change("end_time")}
          className="form-control customModel"
          required
        />
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Create Meeting
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderSuccessMessage() {
    let numberOfSelected = this.state.selectFromArray.filter(
      item => item.checked
    );
    return (
      <Msg
        title="Committee Members have been saved Successfully"
        subTitle={numberOfSelected.length + " Members in the Committee"}
      />
    );
  }

  renderCreationModal() {
    let { newObj } = this.state;
    return (
      <Modal id="createModal" lg>
        {/* {newObj.step == 1 && this.renderCreateObj()} */}
        {newObj.step == 1 && this.renderSelectingModal()}
        {newObj.step == 2 && this.renderSuccessMessage()}
      </Modal>
    );
  }



  gotoView(id) {
    this.resetNewObj();
    this.refreshSubList();
    window.$("#createModal").modal("show");
  }

  renderLink(id, txt) {
    return (
      <td onClick={evt => this.gotoView(id)} className="hand"> {txt} </td>
    );
  }

  viewRow = ({ listItem }) => evt => {
    evt.preventDefault();
    this.gotoView(listItem.id);
  }

  renderTableList() {
    let { list } = this.state;
    return (
      <tbody>
        {list.map((item, index) => {
          let statusClass = "capitilize ";
          if (item.status == "pending") statusClass += "text-muted";
          else if (item.status == "completed") statusClass += "text-success";
          else if (item.status == "inProgress") statusClass += "text-primary";
          return (
            <tr key={index} id={"id-" + item.id}>
              {this.renderLink(item.id, <span> <Img src={item.image} /> {item.name}</span>)}
              {this.renderLink(item.id, item.roleName)}
              {this.renderLink(item.id, item.phone)}
              {this.renderLink(item.id, item.email)}
              <td>{item.op}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }
  renderTableHead() {
    return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Role</th>
          <th>Phone</th>
          <th>Email</th>
          <th>Action</th>
        </tr>
      </thead>
    );
  }
}

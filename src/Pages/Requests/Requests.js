import React, { Component } from "react";
import {
  animateCSS,
  removeClass,
  addClass,
  cloneObj,
  supplierStatusClass,
  updateUserData
} from "../../Helpers/General";
import { Modal, Input, Button, HeaderWithSearch } from "../../Components";
import {
  TableView,
  GenerateTable1,
  GenerateTable2,
  GenerateTableRequests
} from "../TableView";
import { Link } from "react-router-dom";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import { checkPermission } from "../../Helpers/HelperComponenets";
import history from "../../history";
import Checkbox from "../../Components/Checkbox";
import ConfirmationMsg from "../../ConfirmationMsg";

export default class Requests extends Component {
  state = {
    list: [],
    newObj: {
      id: false,
      name: "",
      description: "",
      step: 1,
      supplier: {}
    },
    selectFromArray: [],
    filteringText: "",
    acceptNewRequests: false,
    notifiedUsers: []
  };

  resetNewObj() {
    let newObj = {
      id: false,
      name: "",
      description: "",
      step: 1,
      supplier: {}
    };
    this.setState({ newObj });
  }

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    let promise = Query(`committee/${id}`, "delete");
    promise.then(res => {
      let { list } = this.state;
      animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
        list = list.filter(item => item.id != id);
        this.setState({ list });
      });
    });
  };

  showEditRow = ({ listItem }) => evt => {
    evt.preventDefault();
    let { newObj } = this.state;
    newObj.name = listItem.name;
    newObj.description = listItem.description;
    this.setState({ newObj });
    window.$("#editModal").modal("show");
  };

  editRow = () => evt => {
    evt.preventDefault();
    //TODO: update the backend
    let { name, description } = this.state.newObj;
    let promise = Query("create-committee", "post", { name, description });
    promise.then(results => {
      let promise = Query("committees").then(res => {
        let list = res.data.data;
        list = GenerateTable1(list, this);
        this.setState({ list });
        this.resetNewObj();
        window.$("#editModal").modal("hide");
      });
    });
  };

  addRow = () => evt => {
    evt.preventDefault();
    let { name, description } = this.state.newObj;
    let { newObj } = this.state;
    let promise = Query("create-committee", "post", { name, description });
    promise
      .then(results => {
        let promise = Query("committees").then(res => {
          this.resetNewObj();
          let list = res.data.data;
          list = GenerateTable1(list, this);
          let promise2 = Query("admin-members").then(res => {
            let selectFromArray = res.data.data;
            selectFromArray = GenerateTable2(selectFromArray);
            this.setState({
              list,
              selectFromArray,
              newObj: { ...newObj, step: 2 }
            });
          });
        });
      })
      .catch(this.errorHandling);
  };

  errorHandling(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { newObj, selectFromArray } = this.state;
    let committee = selectFromArray.filter(item => item.checked);
    if (!committee) {
      window.$("#createModal").modal("hide");
      return;
    }

    let committee_id = committee[0].id;
    let supplier_ids = [newObj.supplier.id];

    Query(`supplier/${newObj.id}`, "put", { status: "filtered" }).catch(
      this.errorHandling
    );

    // console.log({ committee_id, supplier: newObj.supplier.id });
    let promise = Query("crud-committee-suppliers", "post", {
      committee_id,
      supplier_ids,
      delete: false
    })
      .then(res => {
        this.refreshList();
        this.setState({ newObj: { ...newObj, step: 2 } });
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
      .catch(this.errorHandling);
    // window.$("#createModal").modal("hide");
  };

  requestAction = (status, request) => evt => {
    evt.preventDefault();
    if (!checkPermission("requests")) return;

    let { newObj } = this.state;
    let { id } = request;
    // console.log({ status, id, request });
    // return;

    if (status == "assign") {
      this.refreshSubList();
      this.setState({ newObj: { ...newObj, id, supplier: request } });
      window.$("#createModal").modal("show");
      return;
    } else if (status == "rejected") {
      Query(`supplier/${id}`, "put", { status: "rejected" }).then(_ => {
        window.location.reload();
      });
      return;
    } else if (status == "filtered") {
      Query(`supplier/${id}`, "put", { status: "filtered" }).then(_ => {
        window.location.reload();
      });
    }
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    let promise;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) item.checked = true;
      else item.checked = false;
      return item;
    });
    this.setState({ selectFromArray });
  };

  refreshList() {
    let promise = Query(`requests`).then(res => {
      let list = res.data.data;
      list = GenerateTableRequests(list, this);
      this.setState({ list });
    });
  }
  refreshSubList() {
    let promise2 = Query("committees").then(res => {
      let selectFromArray = res.data.data;
      selectFromArray = GenerateTable2(selectFromArray);
      this.setState({ selectFromArray });
    });
  }

  refreshList2() {
    let settings = Query(`check-settings`).then(res => {
      let acceptNewRequests = res.data.data[0].status;
      this.setState({ acceptNewRequests });
    });
    let notifiedUser = Query(`notified-users`).then(res => {
      let notifiedUsers = res.data.data;
      this.setState({ notifiedUsers });
    });
  }

  changeAcceptNewSuppliersStatus() {
    let { acceptNewRequests } = this.state;
    let promise = Query(`accept-new-request`, 'post', { status: !acceptNewRequests }).then(res => {
      this.refreshList2();
    });
  }


  async componentDidMount() {
    this.refreshList();
    console.log(localStorage.getItem("requestsCount"), this.props.requestsCount);
    if (localStorage.getItem("requestsCount") != this.props.requestsCount) {
      updateUserData();
      setTimeout(() => { window.location.reload(); }, 1000);
    }
    this.refreshList2();
  }

  render() {
    let { list, acceptNewRequests } = this.state;
    let listLength = list.length || 0;
    return (
      <div className="p-0 m-3 mt-5">

        <ConfirmationMsg id="DisableNewSuppliersConfirmationMsg" msg="Are you sure you want to disable new suppliers from registering ?" confirmed={evt => {
          evt.preventDefault();
          this.changeAcceptNewSuppliersStatus();
        }} />

        {this.renderCreationModal()}
        {this.renderEditModal()}
        <div className="row">
          <Header
            title="Requests"
            icon="bell"
            subTitle={listLength + " Requests"}
          />

          <div className="col-lg-8">
            <div className="float-right">
              {/* <img src="images/arr.png" />
              <img src="images/hed.png" /> */}
              <div className="col-lg-12">
                <ul className="nav float-left customTab nav-pills ">
                  <li className="nav-item">
                    <a
                      className="nav-link active mr-4 text-center"
                      data-toggle="pill"
                      href="#RequestsTable"
                    >
                      <i className="fa fa-bell mr-3"></i>
                      Pending Requests
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link mr-4 text-center"
                      data-toggle="pill"
                      href="#FreezeRequests"
                    >
                      <i className="fa fa-exchange-alt mr-3"></i>
                      Freeze Requests
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div className="tab-pane p-0 fade active show" id="RequestsTable">
          {(list.length <= 0) ?
            <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">No requests found in the system.</h5></div>
            :
            <TableView
              renderTableList={this.renderTableList.bind(this)}
              renderTableHead={this.renderTableHead.bind(this)}
            />
          }
        </div>

        <div className="tab-pane fade p-0" id="FreezeRequests">
          <div className="float-right my-3">
            <h5>
              Accept New Suppliers ? &nbsp;
              <Checkbox value={acceptNewRequests} onChange={evt => {
                evt.preventDefault();
                if (acceptNewRequests) window.$("#DisableNewSuppliersConfirmationMsg").modal("show");
                else this.changeAcceptNewSuppliersStatus()
              }}>
                <span>{acceptNewRequests ? "Yes" : "No"}</span>
              </Checkbox>
            </h5>
          </div>
          {(list.length <= 0) ?
            <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">No Interested Suppliers at the moment.</h5></div>
            :
            <TableView
              renderTableList={this.renderTable2List.bind(this)}
              renderTableHead={this.renderTable2Head.bind(this)}
            />
          }
        </div>

      </div>
    );
  }

  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText } = this.state;
    let { name, description } = this.state.newObj;
    switch (params) {
      case "name":
        name = value;
        break;
      case "description":
        description = value;
        break;
      case "filteringText":
        filteringText = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState({ newObj: { ...newObj, name, description }, filteringText });
  };

  renderEditModal() {
    let { newObj } = this.state;
    return (
      <Modal id="editModal" onSubmit={this.editRow()}>
        <form onSubmit={this.editRow()}>
          <h2 className="pb-4">Edit Committee</h2>
          <Input
            label="Committee Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel"
          />
          <Input
            label="Committee Description"
            type="text"
            value={newObj.description}
            onChange={this.change("description")}
            className="form-control customModel"
          />
          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Save
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  renderSelectingModal() {
    let { selectFromArray, filteringText } = this.state;
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.name) || regexp.test(item.email)
    );
    return (
      <form onSubmit={this.submitSelectingModal()}>
        <HeaderWithSearch
          title="Assign to a committee"
          placeholder="Search Committees"
          value={filteringText}
          onChange={this.change("filteringText")}
        />
        <table className="table mt-2">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Meetings</th>
              <th>Creation date</th>
            </tr>
          </thead>
          <tbody>
            {renderedselectFromArray.map(selectItem => {
              return (
                <tr
                  key={selectItem.id}
                  onClick={this.updateSelectingModal({ selectItem })}
                  className={"hand " + (selectItem.checked ? "checkedTR" : "")}
                >
                  <td>
                    <input
                      type="checkbox"
                      checked={selectItem.checked}
                      onChange={_ => false}
                    />
                  </td>
                  <td>{selectItem.name}</td>
                  <td>{selectItem.meetingsCount}</td>
                  <td>{selectItem.creationDate}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Assign
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderCreateObj() {
    let { newObj } = this.state;
    return (
      <form onSubmit={this.addRow()} className="p-3">
        <h2 className="pb-4">New Committee</h2>
        <Input
          label="Committee Name"
          type="text"
          value={newObj.name}
          onChange={this.change("name")}
          className="form-control customModel"
        />
        <Input
          label="Committee Description"
          type="text"
          value={newObj.description}
          onChange={this.change("description")}
          className="form-control customModel"
        />
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Create And Continue
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderSuccessMessage() {
    return (
      <div className="modal-body p-5">
        <img
          src="images/correct.svg"
          className="imgSuccess mr-auto ml-auto d-block pt-3"
        />
        <h4 className="font-weight-bold text-center pt-5 pb-3">
          Supplier has been assigned successfully
        </h4>
      </div>
    );
  }

  renderCreationModal() {
    let { newObj } = this.state;
    return (
      <Modal id="createModal" lg>
        {newObj.step == 1 && this.renderSelectingModal()}
        {newObj.step == 2 && this.renderSuccessMessage()}
      </Modal>
    );
  }

  renderTd(item, txt) {
    return (
      <td>
        <Link to={`/requests/${item.id}`} className="width100 capitilize">
          {txt}
        </Link>
      </td>
    );
  }



  gotoView(id) {
    history.push({
      pathname: `/requests/${id}`,
      search: ''
    })
  }

  renderLink(id, txt) {
    return (
      <td onClick={evt => this.gotoView(id)} className="hand text-capitalize"> {txt} </td>
    );
  }

  viewRow = ({ listItem }) => evt => {
    evt.preventDefault();
    this.gotoView(listItem.id);
  }

  renderTableList() {
    let { list } = this.state;
    return (
      <tbody>
        {list.map((item, index) => {
          return (
            <tr key={index} id={"id-" + item.id}>
              {this.renderLink(item.id, item.username)}
              {this.renderLink(item.id, item.ActivityNames.join(", "))}
              {this.renderLink(item.id, item.email)}
              {this.renderLink(item.id, item.phone)}
              {this.renderLink(item.id, <span className={supplierStatusClass(item.status)}>{item.status}</span>)}
              <td>{item.op}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }
  renderTableHead() {
    return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Activity</th>
          <th>Email</th>
          <th>Mobile</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
    );
  }


  renderTable2List() {
    let { notifiedUsers } = this.state;
    return (
      <tbody>
        {notifiedUsers.map((item, index) => {
          return (
            <tr key={index}>
              <td>{item.name}</td>
              <td>{item.email}</td>
              <td>{item.phone}</td>
              <td>{item.company_name}</td>
              <td>{item.status ? "Yes" : "No"}</td>
              <td>{item.creation_date}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }
  renderTable2Head() {
    return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Company</th>
          <th>Notified Before</th>
          <th>Added On</th>
        </tr>
      </thead>
    );
  }

}

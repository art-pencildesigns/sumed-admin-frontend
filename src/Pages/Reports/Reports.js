import React, { Component } from "react";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import { random_rgba, animateCSS, printFile } from "../../Helpers/General";
import { Link } from "react-router-dom";

export default class Reports extends Component {
    state = {
        committee: {},
        summary: {}
    };

    async componentDidMount() {
        let id = this.props.match.params.id;
        const promise1 = await Query(`committee/${id}`);
        const committee = promise1.data.data;
        const promise2 = await Query(`committeeSummery/${id}`);
        const summary = promise2.data.data;
        this.setState({ committee, summary });
    };


    render() {
        let { committee, summary } = this.state;
        let index = 55;
        return (
            <div className="p-0 m-3 mt-5">
                <div className="row">
                    <Header
                        icon="handshake"
                        className="col-6"
                        title={committee.name}
                        back="back to Committies"
                        to="/committees"
                    />
                </div>


                <div className="row mt-5">
                    <div className="col-12 mb-3"><h4>Final Status Results</h4></div>
                    <div className="col-4">
                        <div className="shadowBox bg-white radius p-4 bluish">
                            <h1>{summary.meetingsCount}</h1>
                            <p className="font-size2">Number of Meetings</p>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="shadowBox bg-white radius p-4 bluish">
                            <h1>{summary.suppliersCount}</h1>
                            <p className="font-size2">Number of Suppliers</p>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="shadowBox bg-white radius p-4 bluish">
                            <h1>{summary.documentsCount}</h1>
                            <p className="font-size2">Total Documents</p>
                        </div>
                    </div>

                    <div className="col-4 mt-4">
                        <div className="shadowBox bg-white radius p-4 bluish">
                            <h1>{summary.rejectedSuppliersCount}</h1>
                            <p className="font-size2">Number of Rejected Suppliers</p>
                        </div>
                    </div>
                    <div className="col-4 mt-4">
                        <div className="shadowBox bg-white radius p-4 bluish">
                            <h1>{summary.acceptedSuppliersCount}</h1>
                            <p className="font-size2">Number of Accepted Suppliers</p>
                        </div>
                    </div>
                    <div className="col-4 mt-4">
                        <div className="shadowBox bg-white radius p-4 bluish">
                            <h1>{summary.deletedSuppliersCount}</h1>
                            <p className="font-size2">Number of Deleted Suppliers</p>
                        </div>
                    </div>
                </div>





                <div className="row col-12 m-0 p-0 mt-5" id="MeetingsMainArea">
                    <div><h4>Total Meetings</h4></div>

                    {summary.meetings && summary.meetings.map((meeting, index) => {
                        return (<div className="col-12 card border-0 firstUploadCollapse mt-3" key={index}>
                            <div className="card-header border-bottom-0 bg-white py-3 px-2" role="tab">
                                <a data-toggle="collapse" data-parent="#MeetingsMainArea" href={`.Meeting-${index}`} aria-expanded="true" aria-controls="collapseDetails2">
                                    <h5 className="mb-2 hedAccordian border-0">
                                        <div className="row">
                                            <div className="col-3"><span className="text-sm text-primary">Name</span> <br /> <span className="text-dark">{meeting.topic}</span></div>
                                            <div className="col-3"><span className="text-sm text-primary">Date</span> <br /> <span className="text-dark">{meeting.date}</span></div>
                                            <div className="col-3"><span className="text-sm text-primary">Status</span> <br /> <span className="text-dark text-capitalize">{meeting.status}</span></div>
                                            <div className="col-3"><span className="text-sm text-primary">Suppliers</span> <br /> <span className="text-dark">{meeting.rejectedSuppliersCount} Rejected, {meeting.acceptedSuppliersCount} Approved</span></div>

                                        </div>
                                        {/* <i className="float-right fas fa-angle-down rotate-icon" /> */}
                                    </h5>
                                </a>
                            </div>
                            <div className={"collapse borderd border-top-0 pt-0 Meeting-" + index} role="tabpanel" aria-labelledby="headingOne1" data-parent="#AttachmentsBox">
                                <div className="card-body pt-0">
                                    <hr />
                                    <span className="text-primary text-bold">Attached Documents</span>
                                    {meeting.documents.map((file, index2) =>
                                        <div className="row h5 mt-3">
                                            <a className="col-10 text-dark" href={file.path} download={file.name}>{file.name}</a>
                                            <div className="col-2">
                                                {/* <a className="text-dark ml-2" onClick={evt => printFile(file.path)}><i className="fa fa-print fDownload" /></a> */}
                                                <a className="text-dark ml-2" href={file.path} download={file.name}><i className="fa fa-download fDownload" /></a>
                                            </div>
                                        </div>
                                    )}
                                    <hr />
                                    <div className="col-12 p-0">
                                        <span className="text-primary">{meeting.description_header || "Meeting Minutes"}</span>
                                        <pre className="h4 text-dark">{meeting.description}</pre>
                                    </div>
                                </div>
                            </div>


                        </div>)
                    })}


                </div>
            </div >
        );
    }
}

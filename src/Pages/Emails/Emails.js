import React, { Component } from "react";
import {
  animateCSS,
  removeClass,
  addClass,
  cloneObj
} from "../../Helpers/General";
import { Modal, Input } from "../../Components";
import { TableView, GenerateTable1, GenerateTable2 } from "../TableView";
import { Link } from "react-router-dom";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import Block2 from "../Roles/Block2";
import { checkPermission } from "../../Helpers/HelperComponenets";
import history from "../../history";

const emailTypes = [
  "Supplier Activity Approval",
  "Supplier Activity Rejection",
  "Supplier Request Refusal",
  "List Message"
];

class Emails extends Component {
  state = {
    list: [],
    newObj: {
      id: false,
      name: "",
      content: "",
      content_ar: "",
      description: "",
      step: 1,
      category: ""
    },
    selectFromArray: [],
    filteringText: ""
  };

  resetNewObj() {
    let newObj = {
      id: false,
      name: "",
      content: "",
      content_ar: "",
      description: "",
      step: 1,
      category: ""
    };
    this.setState({ newObj });
  }

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    if (!checkPermission("manage")) return;
    let promise = Query(`email-template/${id}`, "delete");
    promise.then(res => {
      let { list } = this.state;
      animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
        list = list.filter(item => item.id != id);
        this.setState({ list });
      });
    }).catch(this.errorHandling);
  };

  showEditRow = ({ listItem }) => evt => {
    evt.preventDefault();
    if (!checkPermission("manage")) return;
    this.gotoView(listItem.id);
  };

  editRow = () => evt => {
    evt.preventDefault();
    let { name, description, id } = this.state.newObj;
    let promise = Query(`committee/${id}`, "put", { name, description });
    promise
      .then(results => {
        this.refreshList();
        this.resetNewObj();
        window.$("#editModal").modal("hide");
      })
      .catch(error => {
        console.error("request failed: ", {
          error,
          response: error.response,
          msg: error.response.data.message
        });
        window.alert(error.response.data.message);
      });
  };

  refreshList() {
    let promise = Query("email-templates?lang=en").then(res => {
      let list = res.data.data;
      list = GenerateTable1(list, this, { edit: true, delete: true });
      this.setState({ list });
    });
  }
  refreshSubList() {
    // let promise2 = Query("admin-members").then(res => {
    //   let selectFromArray = res.data.data;
    //   selectFromArray = GenerateTable2(selectFromArray);
    //   this.setState({ selectFromArray });
    // });
  }

  addRow = () => evt => {
    evt.preventDefault();
    let { newObj } = this.state;
    let { name, category ,content, content_ar} = newObj;
    let promise = Query("create-email-template", "post", {
      name,
      category: category,
      content: content,
      content_ar: content_ar,
      deletable: 0,
      lang: "en"
    });
    promise
      .then(results => {
        // let id = results.data.data.id; //setting the committee id
        this.refreshList();
        this.resetNewObj();
        // this.refreshSubList();
        // this.setState({ newObj: { ...newObj, id, step: 2 } });
        window.$("#createModal").modal("hide");
      })
      .catch(this.errorHandling);
  };

  errorHandling(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { newObj, selectFromArray } = this.state;
    let admin_ids = selectFromArray
      .filter(item => item.checked)
      .map(item => {
        return item.id;
      });
    let promise = Query("crud-committee-member", "post", {
      committee_id: newObj.id,
      admin_ids,
      delete: false
    })
      .then(res => {
        this.refreshList();
        this.setState({ newObj: { ...newObj, step: 3 } });
      })
      .catch(this.errorHandling);
    // window.$("#createModal").modal("hide");
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) item.checked = !item.checked;
      return item;
    });
    this.setState({ selectFromArray });
  };

  async componentDidMount() {
    this.refreshList();
    // animateCSS("#RightContent", "bounceOutRight", _ => {
    //   animateCSS("#RightContent", "bounceInRight");
    // });
  }

  render() {
    let { list } = this.state;
    let listLength = list.length || 0;

    return (
      <div className="p-0 m-3 mt-5">
        {this.renderCreationModal()}
        {this.renderEditModal()}
        <div className="row">
          <Header
            icon="envelope-square"
            title="Email Templates"
            subTitle={listLength + " Templates"}
          />

          <div className="col-lg-8">
            <div className="float-right">
              <span className="ml-5">
                <button
                  className="btn btn-outline-suceess btnCreate mt-0 align-middle"
                  onClick={evt => {
                    evt.preventDefault();
                    if (!checkPermission("manage")) return;
                    window.$("#createModal").modal("show");
                  }}
                >
                  <img src="images/plus.png" /> New Template
                </button>
              </span>
            </div>
            <Block2 active="emails" />
          </div>





        </div>

        {(list.length <= 0) ?
          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no saved emails</h5></div>
          :
          <TableView
            renderTableList={this.renderTableList.bind(this)}
            renderTableHead={this.renderTableHead.bind(this)}
          />
        }
      </div>
    );
  }

  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText } = this.state;
    let { name, description, category, content, content_ar } = this.state.newObj;
    switch (params) {
      case "name":
        name = value;
        break;
      case "description":
        description = value;
        break;
      case "content":
        content = value;
        break;
      case "content_ar":
        content_ar = value;
        break;
      case "category":
        category = value;
        break;
      case "filteringText":
        filteringText = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState({
      newObj: { ...newObj, name, description, content, content_ar, category },
      filteringText
    });
  };

  renderEditModal() {
    let { newObj } = this.state;
    return (
      <Modal id="editModal" onSubmit={this.editRow()}>
        <form onSubmit={this.editRow()}>
          <h2 className="pb-4">Edit Email Template</h2>
          <Input
            label="Template Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel"
          />
          <Input
            label="English Content"
            type="text"
            value={newObj.content}
            onChange={this.change("content")}
            className="form-control customModel"
          />
          <Input
            label="المحتوى العربى"
            type="text"
            value={newObj.content_ar}
            onChange={this.change("content_ar")}
            className="form-control customModel"
          />

          <div className="form-group mt-4">
            <label>
              <b>Category: {newObj.category}</b>
            </label>
          </div>

          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Save
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  renderCreateObj() {
    let { newObj } = this.state;
    return (
      <form onSubmit={this.addRow()} className="p-3">
        <h2 className="pb-4">New Email Template</h2>
        <Input
          label="Template Name"
          type="text"
          value={newObj.name}
          onChange={this.change("name")}
          className="form-control customModel"
          required
        />
        <Input
            label="English Content"
            type="text"
            value={newObj.content}
            onChange={this.change("content")}
            className="form-control customModel"
          />
          <Input
            label="المحتوى العربى"
            type="text"
            value={newObj.content_ar}
            onChange={this.change("content_ar")}
            className="form-control customModel"
          />

        <div className="form-group mt-4">
          <label>
            <b>Category</b>
          </label>
          <select
            className="form-control customModel"
            value={newObj.category}
            onChange={this.change("category")}
            required
          >
            <option disabled value=""> Select Case </option>
            {emailTypes.map((cate, index) => <option key={index} value={cate}>{cate}</option>)}
          </select>
        </div >

        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Create Email Template
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form >
    );
  }

  renderSuccessMessage() {
    let numberOfSelected = this.state.selectFromArray.filter(
      item => item.checked
    );
    return (
      <div className="modal-body p-5">
        <img
          src="images/correct.svg"
          className="imgSuccess mr-auto ml-auto d-block pt-3"
        />
        <h4 className="font-weight-bold text-center pt-5 pb-3">
          Meeting has been added successfully
        </h4>
        <p className="text-center">
          <small className="font-weight-bold text-muted">
            {numberOfSelected.length} Members in the Committee
          </small>
        </p>
      </div>
    );
  }

  renderCreationModal() {
    let { newObj } = this.state;
    return (
      <Modal id="createModal">
        {newObj.step == 1 && this.renderCreateObj()}
        {newObj.step == 2 && this.renderSuccessMessage()}
      </Modal>
    );
  }



  gotoView(id) {
    history.push({
      pathname: `/emails/${id}`,
      search: ''
    })
  }

  renderLink(id, txt) {
    return (
      <td onClick={evt => this.gotoView(id)} className="hand"> {txt} </td>
    );
  }

  viewRow = ({ listItem }) => evt => {
    evt.preventDefault();
    this.gotoView(listItem.id);
  }

  renderTableList() {
    let { list } = this.state;
    return (
      <tbody>
        {list.map((item, index) => {
          return (
            <tr key={index} id={"id-" + item.id}>
              {this.renderLink(item.id, item.name)}
              {this.renderLink(item.id, item.category)}
              <td>{item.op}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }
  renderTableHead() {
    return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Category</th>
          <th>Actions</th>
        </tr>
      </thead>
    );
  }
}

export default Emails;

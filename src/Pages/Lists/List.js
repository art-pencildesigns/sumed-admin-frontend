import React, { Component } from "react";
import {
  animateCSS,
  removeClass,
  addClass,
  cloneObj,
  supplierStatusClass
} from "../../Helpers/General";
import { Modal, Input, Button, HeaderWithSearch } from "../../Components";
import {
  TableView,
  GenerateTable1,
  GenerateTable2,
  GenerateTable3
} from "../TableView";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import { checkPermission } from "../../Helpers/HelperComponenets";
import history from "../../history";

class List extends Component {
  state = {
    list: [],
    newObj: {
      id: false,
      name: "",
      description: "",
      step: 1
      // commercial_name:"",
      // tax_number:null,
      // company_name:""
    },
    selectFromArray: [],
    filteringText: "",
    emailTemplates: [],
    emailMsg_en: "",
    emailMsg_ar: "",
    additionalInfo: {}
  };
  id = false;

  resetNewObj() {
    let newObj = {
      id: false,
      name: "",
      description: "",
      step: 1
    };
    this.setState({ newObj });
  }

  refreshList() {
    let id = (this.id = this.props.match.params.id);
    let additionalInfo = {};
    let promise = Query(`list/${id}`).then(res => {
      res = res.data.data;
      console.log({ res });
      additionalInfo = {
        id: res.id,
        name: res.name,
        description: res.description
      };
      let list = res.suppliers;
      list = GenerateTable1(list, this, { delete: true });
      console.log({ list, additionalInfo });
      this.setState({ list, additionalInfo });
    });
  }
  refreshSubList() {
    let { list } = this.state;
    let promise2 = Query("suppliers").then(res => {
      let selectFromArray = res.data.data;
      let inThereMap = list.map(item => item.id);
      selectFromArray = GenerateTable3(selectFromArray, inThereMap);
      this.setState({ selectFromArray });
    });
  }

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    let promise = Query(`crud-list-supplier`, "post", {
      list_id: this.state.additionalInfo.id,
      supplier_ids: [id],
      delete: true
    });
    promise.then(res => {
      let { list } = this.state;
      animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
        list = list.filter(item => item.id != id);
        this.setState({ list });
      });
    }).catch(this.handleErrors);
  };

  handleErrors(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  showEditRow = ({ listItem }) => evt => {
    evt.preventDefault();
    let { newObj } = this.state;
    newObj.name = listItem.name;
    newObj.description = listItem.description;
    this.setState({ newObj });
    window.$("#editModal").modal("show");
  };

  editRow = () => evt => {
    evt.preventDefault();
    //TODO: update the backend
    let { name, description } = this.state.newObj;
    let promise = Query("create-committee", "post", { name, description });
    promise.then(results => {
      let promise = Query("committees").then(res => {
        let list = res.data.data;
        list = GenerateTable1(list, this);
        this.setState({ list });
        this.resetNewObj();
        window.$("#editModal").modal("hide");
      });
    });
  };

  addRow = () => evt => {
    evt.preventDefault();
    let { name, description } = this.state.newObj;
    let { newObj } = this.state;
    let promise = Query("create-committee", "post", { name, description });
    promise.then(results => {
      let promise = Query("committees").then(res => {
        this.resetNewObj();
        let list = res.data.data;
        list = GenerateTable1(list, this);
        let promise2 = Query("admin-members").then(res => {
          let selectFromArray = res.data.data;
          selectFromArray = GenerateTable2(selectFromArray);
          this.setState({
            list,
            selectFromArray,
            newObj: { ...newObj, step: 2 }
          });
        });
      });
    });
  };
  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { additionalInfo, selectFromArray, newObj } = this.state;
    let supplier_ids = selectFromArray
      .filter(item => item.checked)
      .map(item => {
        return item.id;
      });
    let promise = Query("crud-list-supplier", "post", {
      list_id: additionalInfo.id,
      supplier_ids,
      delete: false
    }).then(res => {
      this.refreshList();
      this.setState({ newObj: { ...newObj, step: 2 } });
    });
    // window.$("#createModal").modal("hide");
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) item.checked = !item.checked;
      return item;
    });
    this.setState({ selectFromArray });
  };

  async componentDidMount() {
    this.refreshList();

    Query("email-templates", "get").then(res => {
      let emailTemplates = res.data.data;
      emailTemplates = emailTemplates.filter(temp => {
        if (temp.category == "List Message") return true;
        return false;
      });
      this.setState({ emailTemplates })
    }).catch(this.errorHandling)
    // animateCSS("#RightContent", "bounceOutRight", _ => {
    //   animateCSS("#RightContent", "bounceInRight");
    // });
  }

  render() {
    let { list, additionalInfo } = this.state;
    if (!additionalInfo) return <div></div>;

    return (
      <div className="p-0 m-3 mt-5">
        {this.renderCreationModal()}
        {this.renderEditModal()}
        {this.renderSelectFromEmailTemplates()}
        <div className="row">
          <Header
            icon="clipboard-list"
            title={additionalInfo.name}
            back="Back"
          />

          <div className="col-lg-8">
            <div className="float-right">
              {/* <img src="images/arr.png" />
                <img src="images/hed.png" /> */}
              <span className="ml-5">
                <button
                  className="btn btn-outline-suceess btnCreate mt-0 align-middle mr-2"
                  onClick={evt => {
                    evt.preventDefault();
                    // if (!checkPermission("lists")) return;
                    // this.resetNewObj();
                    // this.refreshList();
                    // this.refreshSubList();
                    window.$("#selectFromEmailTemplates").modal("show");
                  }}
                >
                  <i className="fa fa-share-square mr-1"></i> Send Email to List
                </button>

                <button
                  className="btn btn-outline-suceess btnCreate mt-0 align-middle"
                  onClick={evt => {
                    evt.preventDefault();
                    if (!checkPermission("lists")) return;

                    this.resetNewObj();
                    this.refreshList();
                    this.refreshSubList();
                    window.$("#createModal").modal("show");
                  }}
                >
                  <i className="fa fa-plus-square mr-1"></i> Add Suppliers
                </button>
              </span>
            </div>
          </div>
        </div>

        <div className="radius shadowBox card p-4 pb-0 mt-4">
          <h6>
            <b>Description</b>
          </h6>
          <p>{additionalInfo.description}</p>
        </div>

        <TableView
          renderTableList={this.renderTableList.bind(this)}
          renderTableHead={this.renderTableHead.bind(this)}
        />
      </div>
    );
  }


  renderSelectFromEmailTemplates() {
    let { emailTemplates, emailMsg_en, emailMsg_ar } = this.state;
    return <Modal id="selectFromEmailTemplates">
      <h3 className="mb-4">Choose an Email Template:</h3>
      {emailTemplates.length >= 1 && emailTemplates.map((temp, index) =>
        <button className="btn btn-outline-primary btn-block" key={index} onClick={evt => {
          this.setState({ emailMsg_en: temp.content, emailMsg_ar: temp.content_ar })
        }}>{temp.name}</button>
      )}

      <label className=" mt-4">Message in English</label>
      <textarea rows="5" value={emailMsg_en} onChange={this.change("emailMsg_en")} className="form-control"></textarea>
      <label className=" mt-2">Message in Arabic</label>
      <textarea rows="5" value={emailMsg_ar} onChange={this.change("emailMsg_ar")} className="form-control"></textarea>

      <div className="modal-footer d-block px-5 border-top-0">
        <button
          type="button"
          className="bg-white text-muted border-0"
          data-dismiss="modal"
        >
          Cancel
            </button>
        <button
          className="btn btnContinue float-right"
          onClick={this.sendEmail()}
        >
          Submit
              <i className="fa fa-arrow-circle-right ml-4"></i>
        </button>
      </div>
    </Modal>
  }

  sendEmail = params => evt => {
    evt.preventDefault();
    let { emailMsg_en, emailMsg_ar, additionalInfo } = this.state;
    let emailObj = {
      list_id: additionalInfo.id,
      message_en: emailMsg_en,
      message_ar: emailMsg_ar,
      files: []
    };
    Query("sendemails", "post", emailObj).then(res => {
      console.log({ res });
      window.$("#selectFromEmailTemplates").modal("hide");
    }).catch(this.errorHandling);
  }


  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText, emailMsg_en, emailMsg_ar } = this.state;
    // let { name, description , commercial_name, tax_number ,company_name } = this.state.newObj;
    let { name, description } = this.state.newObj;
    switch (params) {
      case "name":
        name = value;
        break;
      case "description":
        description = value;
        break;
      // case "commercial_name":
      //   commercial_name = value;
      //   break;
      // case "company_name":
      //   company_name = value;
      //   break;
      // case "tax_number":
      //   tax_number = value;
      //   break;
      
      case "filteringText":
        filteringText = value;
        break;
      case "emailMsg_en": emailMsg_en = value; break;
      case "emailMsg_ar": emailMsg_ar = value; break;
      default:
        console.error("something went wrong");
    }
    this.setState({ newObj: { ...newObj, name, description }, filteringText, emailMsg_en, emailMsg_ar });
  };

  renderEditModal() {
    let { newObj } = this.state;
    return (
      <Modal id="editModal" onSubmit={this.editRow()} lg>
        <form onSubmit={this.editRow()}>
          <h2 className="pb-4">Edit Committee</h2>
          <Input
            label="Committee Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel"
          />
          <Input
            label="Committee Description"
            type="text"
            value={newObj.description}
            onChange={this.change("description")}
            className="form-control customModel"
          />
          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              className="btn btnContinue float-right"
              type="submit"
            >
              Save
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  renderSelectingModal() {
    let { selectFromArray, filteringText } = this.state;
    console.log({ selectFromArray });
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.username) || regexp.test(item.email) || regexp.test(item.company_name) ||
      regexp.test(item.tax_number) || regexp.test(item.commercial_name)
    );
    return (
      <form onSubmit={this.submitSelectingModal()}>
        <HeaderWithSearch
          title="Add Suppliers"
          value={filteringText}
          onChange={this.change("filteringText")}
        />
        <div className="table-responsive">
          <table className="table mt-2">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Company Name</th>
                <th>Phone</th>
                <th>Commercial Name</th>
                <th>Tax Number</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {renderedselectFromArray.map(selectItem => {
                return (
                  <tr
                    key={selectItem.id}
                    onClick={this.updateSelectingModal({ selectItem })}
                    className={"hand " + (selectItem.checked ? "checkedTR" : "")}
                  >
                    <td>
                      <input
                        type="checkbox"
                        checked={selectItem.checked}
                        onChange={_ => false}
                      />
                    </td>
                    <td>{selectItem.username}</td>
                    <td>{selectItem.company_name}</td>
                    <td>{selectItem.phone}</td>
                    <td>{selectItem.commercial_name}</td>
                    <td>{selectItem.tax_number}</td>
                    <td>{selectItem.email}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            className="btn btnContinue float-right"
            type="submit"
          >
            Save List Suppliers
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  // renderCreateObj() {
  //   let { newObj } = this.state;
  //   return (
  //     <form onSubmit={this.addRow()} className="p-3">
  //       <h2 className="pb-4">New Committee</h2>
  //       <Input
  //         label="Committee Name"
  //         type="text"
  //         value={newObj.name}
  //         onChange={this.change("name")}
  //         className="form-control customModel"
  //       />
  //       <Input
  //         label="Committee Description"
  //         type="text"
  //         value={newObj.description}
  //         onChange={this.change("description")}
  //         className="form-control customModel"
  //       />
  //       <div className="modal-footer d-block px-5 border-top-0">
  //         <button
  //           type="button"
  //           className="bg-white text-muted border-0"
  //           data-dismiss="modal"
  //         >
  //           Cancel
  //         </button>
  //         <button
  //           type="button"
  //           className="btn btnContinue float-right"
  //           type="submit"
  //         >
  //           Create And Continue
  //           <i className="fa fa-arrow-circle-right ml-4"></i>
  //         </button>
  //       </div>
  //     </form>
  //   );
  // }

  renderSuccessMessage() {
    let numberOfSelected = this.state.selectFromArray.filter(
      item => item.checked
    );
    return (
      <div className="modal-body p-5">
        <img
          src="images/correct.svg"
          className="imgSuccess mr-auto ml-auto d-block pt-3"
        />
        <h4 className="font-weight-bold text-center pt-5 pb-3">
          Suppliers Have been modified successfully
        </h4>
        <p className="text-center">
          <small className="font-weight-bold text-muted">
            {numberOfSelected.length} Suppliers in the List
          </small>
        </p>
      </div>
    );
  }

  renderCreationModal() {
    let { newObj } = this.state;
    return (
      <Modal id="createModal" lg>
        {/*  {newObj.step == 1 && this.renderCreateObj()} */}
        {newObj.step == 1 && this.renderSelectingModal()}
        {newObj.step == 2 && this.renderSuccessMessage()}
      </Modal>
    );
  }


  renderTD(item, txt) {
    return <td className='text-capitalize hand' onClick={evt => {
      evt.preventDefault();
      history.push({ pathname: "/suppliers/" + item.id })
    }}>{txt}</td>
  }

  renderTableList() {
    let { list } = this.state;
    return (
      <tbody>
        {list.map((item, index) => {
          return (
            <tr key={index} id={"id-" + item.id}>
              {this.renderTD(item, item.username)}
              {this.renderTD(item, item.ActivityNames.join(", "))}
              {this.renderTD(item, item.type)}
              {this.renderTD(item, item.date)}
              {this.renderTD(item, <span className={supplierStatusClass(item.status)}>{item.status}</span>)}
              <td>{item.op}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }
  renderTableHead() {
    return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Activities</th>
          <th>Type</th>
          <th>Joining Date</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
    );
  }
}

export default List;

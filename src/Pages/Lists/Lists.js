import React, { Component } from "react";
import {
  animateCSS,
  removeClass,
  addClass,
  cloneObj
} from "../../Helpers/General";
import { Modal, Input, Button, Msg, HeaderWithSearch } from "../../Components";

import { TableView, GenerateTable1, GenerateTable2,GenerateTable3 } from "../TableView";
import { Link } from "react-router-dom";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import { checkPermission } from "../../Helpers/HelperComponenets";
import history from "../../history";

class Lists extends Component {
  state = {
    additionalInfo: {
      name: "",
      description: ""
    },
    list: [],
    newObj: {
      id: "",
      name: "",
      description: "",
      step: 1
    },
    selectFromArray: [],
    filteringText: ""
  };

  resetNewObj() {
    let newObj = {
      id: false,
      name: "",
      description: "",
      selectFromArray: [],
      step: 1
    };
    this.setState({ newObj });
  }

  refreshList() {
    let promise = Query("lists").then(res => {
      let list = res.data.data;
      list = GenerateTable1(list, this);
      this.setState({ list });
    });
  }

  refreshSubList() {
    let promise2 = Query("suppliers").then(res => {
      let selectFromArray = res.data.data;
      selectFromArray = GenerateTable2(selectFromArray);
      this.setState({ selectFromArray });
    });
  }

  errorHandling(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    if (!checkPermission("lists")) return;

    let promise = Query(`list/${id}`, "delete");
    promise.then(res => {
      let { list } = this.state;
      animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
        list = list.filter(item => item.id != id);
        this.setState({ list });
      });
    });
  };

  showEditRow = ({ listItem }) => evt => {
    evt.preventDefault();
    if (!checkPermission("lists")) return;

    let { newObj } = this.state;
    newObj.id = listItem.id;
    newObj.name = listItem.name;
    newObj.description = listItem.description;
    this.setState({ newObj });
    window.$("#editModal").modal("show");
  };

  editRow = () => evt => {
    evt.preventDefault();
    let { name, description, id } = this.state.newObj;
    let promise = Query(`list/${id}`, "put", { name, description });
    promise
      .then(results => {
        this.refreshList();
        window.$("#editModal").modal("hide");
      })
      .catch(this.errorHandling);
  };

  addRow = () => evt => {
    evt.preventDefault();
    let { name, description } = this.state.newObj;
    let { newObj } = this.state;
    let promise = Query("create-list", "post", { name, description });
    promise
      .then(results => {
        let id = results.data.data.id; //setting the committee id
        this.resetNewObj();
        setTimeout(() => {
          this.refreshList();
          this.refreshSubList();
          this.setState({ newObj: { ...newObj, id, step: 2 } });
        }, 300);
      })
      .catch(this.errorHandling);
  };

  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { newObj, selectFromArray } = this.state;
    let supplier_ids = selectFromArray
      .filter(item => item.checked)
      .map(item => {
        return item.id;
      });
    let promise = Query("crud-list-supplier", "post", {
      list_id: newObj.id,
      supplier_ids,
      delete: false
    }).then(res => {
      this.refreshList();
      this.setState({ newObj: { ...newObj, step: 3 } });
    });
    // window.$("#createModal").modal("hide");
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) item.checked = !item.checked;
      return item;
    });
    this.setState({ selectFromArray });
  };

  async componentDidMount() {
    this.refreshList();
    // animateCSS("#RightContent", "bounceOutRight", _ => {
    //   animateCSS("#RightContent", "bounceInRight");
    // });
  }

  render() {
    let { list } = this.state;
    let listLength = list.length || 0;

    return (
      <div className="p-0 m-3 mt-5">
        {this.renderCreationModal()}
        {this.renderEditModal()}
        <div className="row">
          <Header
            icon="clipboard-list"
            title="Lists"
            subTitle={listLength + " Lists"}
          />

          <div className="col-lg-8">
            <div className="float-right">
              {/* <img src="images/arr.png" />
              <img src="images/hed.png" /> */}
              <span className="ml-5">
                <button
                  className="btn btn-outline-suceess btnCreate mt-0"
                  onClick={evt => {
                    evt.preventDefault();
                    if (!checkPermission("lists")) return;

                    this.resetNewObj();
                    window.$("#createModal").modal("show");
                  }}
                >
                  <i className="fa fa-plus-square mr-1"></i> Create List
                </button>
              </span>
            </div>
          </div>
        </div>

        {(list.length <= 0) ?
          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no meetings yet</h5></div>
          :
          <TableView
            renderTableList={this.renderTableList.bind(this)}
            renderTableHead={this.renderTableHead.bind(this)}
          />
        }
      </div>
    );
  }

  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText } = this.state;
    let { name, description } = this.state.newObj;
    switch (params) {
      case "name":
        name = value;
        break;
      case "description":
        description = value;
        break;
      case "filteringText":
        filteringText = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState({
      newObj: { ...newObj, name, description },
      filteringText
    });
  };

  renderEditModal() {
    let { newObj } = this.state;
    return (
      <Modal id="editModal" onSubmit={this.editRow()}>
        <form onSubmit={this.editRow()}>
          <h2 className="pb-4">Edit List</h2>
          <Input
            label="List Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel"
          />
          <Input
            label="List Description"
            type="text"
            value={newObj.description}
            onChange={this.change("description")}
            className="form-control customModel"
          />
          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Save
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  renderSelectingModal() {
    let { selectFromArray, filteringText } = this.state;
    console.log({ selectFromArray });
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.name) || regexp.test(item.email)
    );
    return (
      <form onSubmit={this.submitSelectingModal()}>
        <HeaderWithSearch
          title="Add Suppliers"
          value={filteringText}
          onChange={this.change("filteringText")}
        />
        <div class="table table-responsive">
          <table className="table mt-2">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {renderedselectFromArray.map(selectItem => {
                return (
                  <tr
                    key={selectItem.id}
                    onClick={this.updateSelectingModal({ selectItem })}
                    className={"hand " + (selectItem.checked ? "checkedTR" : "")}
                  >
                    <td>
                      <input
                        type="checkbox"
                        checked={selectItem.checked}
                        onChange={_ => false}
                      />
                    </td>
                    <td>{selectItem.username}</td>
                    <td>{selectItem.phone}</td>
                    <td>{selectItem.email}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Add Later
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Add
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderCreateObj() {
    let { newObj } = this.state;
    return (
      <form onSubmit={this.addRow()} className="p-3">
        <h2 className="pb-4">New List</h2>
        <Input
          label="List Name"
          type="text"
          value={newObj.name}
          onChange={this.change("name")}
          className="form-control customModel"
        />
        <Input
          label="List Description"
          type="text"
          value={newObj.description}
          onChange={this.change("description")}
          className="form-control customModel"
        />
        <div className="modal-footer d-block pt-4 px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Create And Continue
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderSuccessMessage() {
    let numberOfSelected = this.state.selectFromArray.filter(
      item => item.checked
    );
    return <Msg title="Suppliers has been added to List successfully" />;
  }

  renderCreationModal() {
    let { newObj } = this.state;
    return (
      <Modal id="createModal" lg>
        {newObj.step == 1 && this.renderCreateObj()}
        {newObj.step == 2 && this.renderSelectingModal()}
        {newObj.step == 3 && this.renderSuccessMessage()}
      </Modal>
    );
  }



  gotoView(id) {
    history.push({
      pathname: '/list/' + id,
      search: ''
    })
  }

  renderLink(id, txt) {
    return (
      <td onClick={evt => this.gotoView(id)} className="hand"> {txt} </td>
    );
  }

  viewRow = ({ listItem }) => evt => {
    evt.preventDefault();
    this.gotoView(listItem.id);
  }

  renderTableList() {
    let { list } = this.state;
    return (
      <tbody>
        {list.map((item, index) => {
          const suppliersNum = (item.suppliersCount || "No") + " Suppliers";
          return (
            <tr key={index} id={"id-" + item.id}>
              {this.renderLink(item.id, item.name)}
              {this.renderLink(item.id, item.creationDate)}
              {this.renderLink(item.id, suppliersNum)}
              {this.renderLink(item.id, item.description)}
              <td>{item.op}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }
  renderTableHead() {
    return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Creation Date</th>
          <th>Num of Suppliers</th>
          <th>Description</th>
          <th>Actions</th>
        </tr>
      </thead>
    );
  }
}

export default Lists;

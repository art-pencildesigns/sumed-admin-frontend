import React, { Component } from "react";
import {
  animateCSS,
  removeClass,
  addClass,
  cloneObj
} from "../../Helpers/General";
import { Modal, Input, Button, HeaderWithSearch, Msg } from "../../Components";
import { TableView, GenerateTable1 } from "../TableView";
import Header from "../Header";
import { Query } from "../../Helpers/Constants";
import Block2 from "./Block2";
import { checkPermission } from "../../Helpers/HelperComponenets";

let arr = [
  {
    name: "Admin",
    request: true,
    log: true,
    reports: false,
    committees: true,
    lists: false,
    emails: false
  },
  {
    name: "Sales",
    request: false,
    log: false,
    reports: false,
    committees: true,
    lists: false,
    emails: false
  }
];

export default class Roles extends Component {
  state = {
    list: [],
    newObj: {
      id: false,
      name: "",
      description: "",
      step: 1,
      roles: {
        requests: {
          access: true,
          edit: true
        },
        log: {
          access: true,
          edit: true
        },
        reports: {
          access: true,
          edit: true
        },
        committees: {
          access: true,
          edit: true
        },
        lists: {
          access: true,
          edit: true
        },
        emails: {
          access: true,
          edit: true
        }
      }
    },
    selectFromArray: [],
    filteringText: ""
  };

  resetNewObj() {
    let newObj = {
      id: false,
      name: "",
      description: "",
      step: 1,
      roles: {
        requests: {
          access: true,
          edit: true
        },
        log: {
          access: true,
          edit: true
        },
        reports: {
          access: true,
          edit: true
        },
        committees: {
          access: true,
          edit: true
        },
        lists: {
          access: true,
          edit: true
        },
        emails: {
          access: true,
          edit: true
        }
      }
    };
    this.setState({ newObj });
  }

  errorHandling(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    this.refreshList();
    window.alert(error.response.data.message);
  }

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    if (!checkPermission("manage")) return;

    let promise = Query(`role/${id}`, "delete");
    promise.then(res => {
      let { list } = this.state;
      animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
        list = list.filter(item => item.id != id);
        this.setState({ list });
      });
    }).catch(this.errorHandling.bind(this));
  };

  showEditRow = ({ listItem }) => evt => {
    evt.preventDefault();
    let { newObj } = this.state;
    newObj.id = listItem.id;
    newObj.name = listItem.name;
    newObj.description = listItem.description;
    this.setState({ newObj });
    window.$("#editModal").modal("show");
  };

  editRow = () => evt => {
    evt.preventDefault();
    //TODO: update the backend
    let { name, description, id } = this.state.newObj;
    let promise = Query(`committee/${id}`, "put", { name, description });
    promise
      .then(results => {
        this.refreshList();
        this.resetNewObj();
        window.$("#editModal").modal("hide");
      })
      .catch(this.errorHandling);
  };

  refreshList() {
    let promise = Query("admin-roles").then(res => {
      let list = res.data.data;
      list = GenerateTable1(list, this, { delete: true });
      this.setState({ list });
    });
  }

  addRow = () => evt => {
    console.log("add row man");
    evt.preventDefault();
    let { name, description } = this.state.newObj;
    let { newObj } = this.state;
    let promise = Query("create-update-role", "post", {
      ...newObj
    });
    promise
      .then(results => {
        this.refreshList();
        this.setState({ newObj: { ...newObj, step: 3 } });
        // window.$("#createModal").modal("hide");
        // animateCSS("#RightContent", "bounceOutRight", _ => {
        //   animateCSS("#RightContent", "bounceInRight");
        // });
      })
      .catch(error => {
        console.error("request failed: ", {
          error,
          response: error.response,
          msg: error.response.data.message
        });
        window.alert(error.response.data.message);
      });
  };

  submitSelectingModal = params => evt => {
    // evt.preventDefault();
    // let { newObj, selectFromArray } = this.state;
    // let admin_ids = selectFromArray
    //   .filter(item => item.checked)
    //   .map(item => {
    //     return item.id;
    //   });
    // let promise = Query("crud-committee-member", "post", {
    //   committee_id: newObj.id,
    //   admin_ids,
    //   delete: false
    // }).then(res => {
    //   this.refreshList();
    //   this.setState({ newObj: { ...newObj, step: 3 } });
    // }).catch(this.handleCheck);
    // window.$("#createModal").modal("hide");
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) item.checked = !item.checked;
      return item;
    });
    this.setState({ selectFromArray });
  };

  async componentDidMount() {
    this.refreshList();
    // animateCSS("#RightContent", "bounceOutRight", _ => {
    //   animateCSS("#RightContent", "bounceInRight");
    // });
  }

  render() {
    let { list } = this.state;

    return (
      <div className="p-0 m-3 mt-5">
        {this.renderCreationModal()}
        <div className="row">
          <Header icon="user-shield" title="Roles" />

          <div className="col-lg-8">
            <div className="float-right">
              {/* <img src="images/arr.png" />
              <img src="images/hed.png" /> */}
              <span className="ml-5">
                <button
                  className="btn btn-outline-suceess btnCreate mt-0 align-middle"
                  onClick={evt => {
                    evt.preventDefault();
                    if (!checkPermission("manage")) return;
                    this.resetNewObj();
                    window.$("#createModal").modal("show");
                  }}
                >
                  <i className="fa fa-plus-square mr-1"></i> Create New Role
                </button>
              </span>
            </div>
            <Block2 active="roles" />

          </div>
        </div>

        <TableView
          renderTableList={this.renderTableList.bind(this)}
          renderTableHead={this.renderTableHead.bind(this)}
        />
      </div>
    );
  }

  change = params => evt => {
    console.log("h");
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText } = this.state;
    let { name, description } = this.state.newObj;
    switch (params) {
      case "name":
        name = value;
        break;
      case "description":
        description = value;
        break;
      case "filteringText":
        filteringText = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState({ newObj: { ...newObj, name, description }, filteringText });
  };

  renderCreateObj() {
    let { newObj } = this.state;
    return (
      <form onSubmit={this.addRow()} className="p-3">
        <h2 className="pb-4">New Role</h2>
        <Input
          label="Role Name"
          type="text"
          value={newObj.name}
          onChange={this.change("name")}
          className="form-control customModel"
          required
        />
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Save
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderSuccessMessage() {
    return <Msg title="Role has been added successfully" />;
  }

  renderCreationModal() {
    let { newObj } = this.state;
    return (
      <Modal id="createModal" lg>
        {newObj.step == 1 && this.renderCreateObj()}
        {/* {newObj.step == 2 && this.renderSelectingModal()} */}
        {newObj.step == 3 && this.renderSuccessMessage()}
      </Modal>
    );
  }

  handleCheck = (item, page, type, index) => evt => {
    evt.preventDefault();
    if (!checkPermission("manage")) return;
    let { list } = this.state;
    // console.log({ item, page, type });
    if (type == "edit") {
      item.roles[page].edit = !item.roles[page].edit;
      if (item.roles[page].edit) item.roles[page].access = true;
    } else {
      item.roles[page].access = !item.roles[page].access;
      if (!item.roles[page].access) item.roles[page].edit = false;
    }
    list[index] = item;
    this.setState({ list });
    let promise = Query(`create-update-role/${item.id}`, "put", {
      ...item
    })
      .then(res => {
        res = res.data.data;
        this.refreshList();
      })
      .catch(this.errorHandling.bind(this));
  };

  permissionBox(item, page, index) {
    return (
      <>
        <label
          htmlFor="Access"
          onClick={this.handleCheck(item, page, "access", index)}
        >
          <input
            type="checkbox"
            checked={item.roles[page].access}
            onChange={_ => { }}
          />
          <span className="ml-2 hand">Access</span>
        </label>
        <br />
        <label
          htmlFor="Access"
          onClick={this.handleCheck(item, page, "edit", index)}
        >
          <input
            type="checkbox"
            checked={item.roles[page].edit}
            onChange={_ => { }}
          />
          <span className="ml-2 hand">Modify</span>
        </label>
      </>
    );
  }

  renderTableList() {
    let { list } = this.state;
    console.log({ list });
    return (
      <tbody>
        {list.map((item, index) => {
          return (
            <tr key={index} id={"id-" + item.id}>
              <td>{item.name}</td>
              <td>{this.permissionBox(item, "suppliers", index)}</td>
              <td>{this.permissionBox(item, "activities", index)}</td>
              <td>{this.permissionBox(item, "lists", index)}</td>
              <td>{this.permissionBox(item, "committees", index)}</td>
              <td>{this.permissionBox(item, "users", index)}</td>
              <td>{this.permissionBox(item, "manage", index)}</td>
              <td>{this.permissionBox(item, "requests", index)}</td>
              <td>{item.op}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }
  renderTableHead() {
    return (
      <thead>
        <tr>
          <th>Role Name</th>
          <th>Suppliers</th>
          <th>Activities</th>
          <th>Lists</th>
          <th>Committees</th>
          <th>Users</th>
          <th>Manage</th>
          <th>Requests</th>
          <th>Actions</th>
        </tr>
      </thead>
    );
  }
}

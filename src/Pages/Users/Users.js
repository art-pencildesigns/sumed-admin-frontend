import React, { Component } from "react";
import { animateCSS } from "../../Helpers/General";
import { Modal, Input, Button, Msg } from "../../Components";
import {
  TableView,
  GenerateTable1,
} from "../TableView";
import { Query } from "../../Helpers/Constants";
import { checkPermission } from "../../Helpers/HelperComponenets";
import Header from "../Header";

class Users extends Component {
  state = {
    additionalInfo: {
      name: "",
      description: ""
    },
    list: [],
    newObj: {
      id: false,
      name: "",
      description: "",
      email: "",
      password: "",
      password_confirmation: "",
      role: "",
      sendEmail: true,
      step: 1,
      phone: "",
      sumed_id: ""
    },
    selectFromArray: [],
    filteringText: "",
    roles: []
  };

  errorHandling(error) {
    console.error("request failed: ", {
      error,
      response: error.response,
      msg: error.response.data.message
    });
    window.alert(error.response.data.message);
  }

  resetNewObj() {
    let newObj = {
      id: false,
      name: "",
      description: "",
      email: "",
      password: "",
      password_confirmation: "",
      role: "",
      sendEmail: true,
      step: 1,
      phone: "",
      sumed_id: ""
    };
    this.setState({ newObj });
  }

  refreshList() {
    let promise = Query("admin-members").then(res => {
      let list = res.data.data;
      list = GenerateTable1(list, this, { edit: true, delete: true });
      this.setState({ list });
    });
  }

  deleteRow = ({ id }) => evt => {
    evt.preventDefault();
    if (!checkPermission("users")) return;
    let promise = Query(`admin/${id}`, "delete");
    promise.then(res => {
      let { list } = this.state;
      animateCSS(`#id-${id}`, "bounceOutLeft", _ => {
        list = list.filter(item => item.id != id);
        this.setState({ list });
      });
    }).catch(this.errorHandling);
  };

  showEditRow = ({ listItem }) => evt => {
    evt.preventDefault();
    if (!checkPermission("users")) return;

    let { newObj } = this.state;
    newObj.id = listItem.id;
    newObj.name = listItem.name;
    newObj.email = listItem.email;
    newObj.password = "";
    newObj.password_confirmation = "";
    newObj.role = listItem.role_id;
    newObj.phone = listItem.phone;
    newObj.sumed_id = listItem.sumed_id;
    console.log({ newObj })
    this.setState({ newObj });
    window.$("#editModal").modal("show");
  };

  editRow = () => evt => {
    evt.preventDefault();
    let { id, name, email, password, password_confirmation, role, phone, sumed_id } = this.state.newObj;
    let editObj = { name, email, password, password_confirmation, role_id: role, sumed_id, sendEmail: 1 };
    if (phone) editObj.phone = phone;
    let promise = Query(`admin/${id}`, "put", editObj);
    promise.then(results => {
      this.resetNewObj();
      this.refreshList()
      window.$("#editModal").modal("hide");
    }).catch(this.errorHandling);
  };

  addRow = () => evt => {
    evt.preventDefault();
    let {
      name,
      email,
      password,
      password_confirmation,
      role,
      sendEmail,
      step,
      phone,
      sumed_id
    } = this.state.newObj;
    let { newObj } = this.state;
    let creationObj = {
      name,
      email,
      password,
      password_confirmation,
      role_id: role,
      sendEmail: 1,
      sumed_id
    };
    if (phone) creationObj.phone = phone;
    let promise = Query("create-admin", "post", creationObj);
    promise
      .then(results => {
        this.refreshList();
        this.resetNewObj();
        this.setState({ newObj: { ...newObj, step: 2 } });
      })
      .catch(this.errorHandling);
  };

  submitSelectingModal = params => evt => {
    evt.preventDefault();
    let { newObj } = this.state;
    this.setState({
      newObj: { ...newObj, step: 3 }
    });
    // window.$("#createModal").modal("hide");
  };

  updateSelectingModal = ({ selectItem }) => evt => {
    let { selectFromArray } = this.state;
    let promise;
    selectFromArray = selectFromArray.map(item => {
      if (item.id == selectItem.id) {
        item.checked = !item.checked;
        let committee_id = 1;
        let admin_id = selectItem.id;

        if (item.checked) {
          promise = Query("add-committee-member", "post", {
            committee_id,
            admin_id
          });
        } else {
          promise = Query(
            `/committee/${committee_id}/member/${admin_id}`,
            "post"
          );
        }
        promise
          .then(res => {
            this.setState({ selectFromArray });
          })
          .catch(error => {
            console.log(error);
            window.alert("Error has happened");
          });
      }
      return item;
    });
  };

  async componentDidMount() {
    this.refreshList();
    let promise = Query("admin-roles").then(res => {
      let roles = res.data.data;
      this.setState({ roles });
    });
    // animateCSS("#RightContent", "bounceOutRight", _ => {
    //   animateCSS("#RightContent", "bounceInRight");
    // });
  }

  render() {
    let { list } = this.state;

    return (
      <div className="p-0 m-3 mt-5">
        {this.renderCreationModal()}
        {this.renderEditModal()}
        <div className="row">
          <Header icon="users-cog" title="Users" />

          <div className="col-lg-8">
            <div className="float-right">
              {/* <img src="images/arr.png" />
              <img src="images/hed.png" /> */}
              <span className="ml-5">
                <button
                  className="btn btn-outline-suceess btnCreate mt-0"
                  onClick={evt => {
                    evt.preventDefault();
                    if (!checkPermission("users")) return;
                    this.resetNewObj();
                    window.$("#createModal").modal("show");
                  }}
                >
                  <i className="fa fa-plus-square mr-1"></i> Create New User
                </button>
              </span>
            </div>
          </div>
        </div>

        {(list.length <= 0) ?
          <div className="p-5 text-center bg-white mt-3 shadowBox radius EmptyListMsg"><h5 className="text-muted">There are no meetings yet</h5></div>
          :
          <TableView
            renderTableList={this.renderTableList.bind(this)}
            renderTableHead={this.renderTableHead.bind(this)}
          />
        }
      </div>
    );
  }

  change = params => evt => {
    evt.preventDefault();
    let value = evt.target.value;
    let { newObj, filteringText } = this.state;
    let {
      name,
      description,
      email,
      password,
      password_confirmation,
      role,
      sendEmail,
      phone,
      sumed_id
    } = this.state.newObj;
    switch (params) {
      case "name":
        name = value;
        break;
      case "description":
        description = value;
        break;
      case "email":
        email = value;
        break;
      case "password":
        password = value;
        break;
      case "password_confirmation":
        password_confirmation = value;
        break;
      case "role":
        role = value;
        break;
      case "sendEmail":
        sendEmail = !!!sendEmail;
        break;
      case "phone":
        phone = value;
        break;
      case "sumed_id":
        sumed_id = value;
        break;
      case "filteringText":
        filteringText = value;
        break;
      default:
        console.error("something went wrong");
    }
    this.setState(
      {
        newObj: {
          ...newObj,
          name,
          description,
          email,
          password,
          password_confirmation,
          role,
          sendEmail,
          phone,
          sumed_id
        },
        filteringText
      },
      _ => {
        console.log(this.state, newObj);
      }
    );
  };

  renderEditModal() {
    let { newObj, roles } = this.state;
    return (
      <Modal id="editModal" lg onSubmit={this.editRow()}>
        <form onSubmit={this.editRow()}>
          <h2 className="pb-4">Edit User</h2>
          <div className="row">

            <Input
              label="User Name"
              type="text"
              value={newObj.name}
              onChange={this.change("name")}
              className="form-control customModel"
              parentClass="col-6"
              required
            />
            <Input
              label="Email"
              type="email"
              value={newObj.email}
              onChange={this.change("email")}
              className="form-control customModel"
              parentClass="col-6"
              required
            />

            <Input
              label="New Password"
              type="password"
              value={newObj.password}
              onChange={this.change("password")}
              className="form-control customModel"
              parentClass="col-6 mt-4"
            />

            <Input
              label="New Password Confirmation"
              type="password"
              value={newObj.password_confirmation}
              onChange={this.change("password_confirmation")}
              className="form-control customModel"
              parentClass="col-6 mt-4"
            />

            <Input
              optional
              label="Phone"
              type="number"
              value={newObj.phone}
              onChange={this.change("phone")}
              className="form-control customModel"
              parentClass="col-6 mt-4"
            />

            <Input
              label="Sumed ID"
              type="text"
              value={newObj.sumed_id}
              onChange={this.change("sumed_id")}
              className="form-control customModel"
              parentClass="col-6 mt-4"
              required
            />

            <div className="form-group col-12 mt-4">
              <label>
                <b>Role</b>
              </label>
              <select
                className="form-control customModel"
                value={newObj.role}
                onChange={this.change("role")}
                required
              >
                <option disabled value="">
                  Select Role
              </option>
                {roles.map(role => {
                  return (
                    <option key={role.id} value={role.id}>
                      {role.name}
                    </option>
                  );
                })}
              </select>
            </div>

            {/* <div className="form-group col-12 mt-1">
              <label onClick={this.change("sendEmail")} className="hand">
                <input
                  type="checkbox"
                  checked={newObj.sendEmail}
                  onChange={this.change("sendEmail")}
                />
                &nbsp; Send Update to the Email ?
              {newObj.sendEmail ? "Yes" : "No"}
              </label>
            </div> */}
          </div>





          <div className="modal-footer d-block px-5 border-top-0">
            <button
              type="button"
              className="bg-white text-muted border-0"
              data-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              className="btn btnContinue float-right"
              type="submit"
            >
              Update
              <i className="fa fa-arrow-circle-right ml-4"></i>
            </button>
          </div>
        </form>
      </Modal>
    );
  }

  renderSelectingModal() {
    let { selectFromArray, filteringText } = this.state;
    console.log({ selectFromArray });
    const regexp = new RegExp(filteringText, "ig");
    let renderedselectFromArray = selectFromArray.filter(
      item => regexp.test(item.name) || regexp.test(item.email)
    );
    return (
      <form onSubmit={this.submitSelectingModal()}>
        <h2 className="mb-4">
          <span className="float-left">Add Suppliers</span>
          <input
            type="text"
            className="form-control float-right"
            placeholder="Search Suppliers"
            value={filteringText}
            onChange={this.change("filteringText")}
          />
        </h2>
        <table className="table mt-2">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Role</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {renderedselectFromArray.map(selectItem => {
              return (
                <tr
                  key={selectItem.id}
                  onClick={this.updateSelectingModal({ selectItem })}
                  className="hand"
                >
                  <td>
                    <input
                      type="checkbox"
                      checked={selectItem.checked}
                      onChange={_ => false}
                    />
                  </td>
                  <td>{selectItem.name}</td>
                  <td>{selectItem.role_name}</td>
                  <td>{selectItem.email}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Add Later
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Add and Invite
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderCreateObj() {
    let { newObj, roles } = this.state;
    return (
      <form onSubmit={this.addRow()} className="p-3">
        <h2 className="pb-4">New User</h2>
        <div className="row">
          <Input
            label="User Name"
            type="text"
            value={newObj.name}
            onChange={this.change("name")}
            className="form-control customModel"
            parentClass="col-6"
            required
          />
          <Input
            label="Email"
            type="email"
            value={newObj.email}
            onChange={this.change("email")}
            className="form-control customModel"
            parentClass="col-6"
            required
          />

          <Input
            label="Password"
            type="password"
            value={newObj.password}
            onChange={this.change("password")}
            className="form-control customModel"
            parentClass="col-6 mt-4"
            required
          />
          <Input
            label="Password Confirmation"
            type="password"
            value={newObj.password_confirmation}
            onChange={this.change("password_confirmation")}
            className="form-control customModel"
            parentClass="col-6 mt-4"
            required
          />

          <Input
            optional
            label="Phone"
            type="number"
            value={newObj.phone}
            onChange={this.change("phone")}
            className="form-control customModel"
            parentClass="col-6 mt-4"
          />

          <Input
            label="Sumed ID"
            type="text"
            value={newObj.sumed_id}
            onChange={this.change("sumed_id")}
            className="form-control customModel"
            parentClass="col-6 mt-4"
            required
          />

          <div className="form-group col-12 mt-4">
            <label>
              <b>Role</b>
            </label>
            <select
              className="form-control customModel"
              value={newObj.role}
              onChange={this.change("role")}
              required
            >
              <option disabled value="">
                Select Role
              </option>
              {roles.map(role => {
                return (
                  <option key={role.id} value={role.id}>
                    {role.name}
                  </option>
                );
              })}
            </select>
          </div>

          {/* <div className="form-group col-12 mt-1">
            <label onClick={this.change("sendEmail")} className="hand">
              <input
                type="checkbox"
                checked={newObj.sendEmail}
                onChange={this.change("sendEmail")}
              />
              &nbsp; Send invitation to the Email ?
              {newObj.sendEmail ? "Yes" : "No"}
            </label>
          </div> */}

        </div>

        <div className="modal-footer d-block px-5 border-top-0">
          <button
            type="button"
            className="bg-white text-muted border-0"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btnContinue float-right"
            type="submit"
          >
            Save & Send
            <i className="fa fa-arrow-circle-right ml-4"></i>
          </button>
        </div>
      </form>
    );
  }

  renderSuccessMessage() {
    let numberOfSelected = this.state.selectFromArray.filter(
      item => item.checked
    );
    return (
      <Msg title="New User added successfully" />
    );
  }

  renderCreationModal() {
    let { newObj } = this.state;
    return (
      <Modal id="createModal" lg>
        {newObj.step == 1 && this.renderCreateObj()}
        {/* {newObj.step == 2 && this.renderSelectingModal()} */}
        {newObj.step == 2 && this.renderSuccessMessage()}
      </Modal>
    );
  }

  renderTableList() {
    let { list } = this.state;
    return (
      <tbody>
        {list.map((item, index) => {
          let roleClass = (item.role_id == 1) ? "bg-dark text-white radius p-1" : "";
          return (
            <tr key={index} id={"id-" + item.id} className="text-capitalize">
              <td>{item.name}</td>
              <td>{item.sumed_id}</td>
              <td><span className={roleClass}>{item.role_name}</span></td>
              <td>{item.phone}</td>
              <td>{item.email}</td>
              <td>{item.op}</td>
            </tr>
          );
        })}
      </tbody>
    );
  }
  renderTableHead() {
    return (
      <thead>
        <tr>
          <th>Name</th>
          <th>Sumed ID</th>
          <th>Role</th>
          <th>Mobile No.</th>
          <th>Email</th>
          <th>Actions</th>
        </tr>
      </thead>
    );
  }
}

export default Users;
